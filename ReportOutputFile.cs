﻿namespace DSAMailMarkConsole
{
    /// <summary>
    /// Permits user of USESortReport to specify the output filenames for the various files that USESortReport extracts from the USE XML Streams
    /// </summary>
    public class ReportOutputFile
    {
        public const string PlanningAnalysis = "PlanningAnalysis";

        public const string BagLabels = "BagLabel";

        public const string LineListing = "LineListing";

        public const string GemmaNational = "Gemma";

        public const string GemmaZonal = "Gemma";

        public const string WhistlBagLabels = "WhistlBagLabel";

        public const string CitipostTntUpload = "CitipostTntUpload";

        public const string WhistlTntUpload = "WhistlTntUpload";

        public string FileDescription { get; set; }

        public string OutputLocation { get; set; }

        public static string GetOutputLocation(ReportOutputFile[] outputs, string fileDescription)
        {
            foreach (var output in outputs)
                if (output.FileDescription == fileDescription)
                    return output.OutputLocation;                   // found the ReportOutputFile with required description so return

            // not specified so return null
            return null;
        }

        public ReportOutputFile(string description, string location)
        {
            FileDescription = description;
            OutputLocation = location;
        }
    }
}
