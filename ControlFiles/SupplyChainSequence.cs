﻿namespace DSAMailMarkConsole.ControlFiles
{
    /// <summary>
    /// Holds SupplyChainSequence number for a Supply Chain Id
    /// </summary>
    public class SupplyChainSequence
    {
        public SupplyChainSequence(string id, int sequence, bool locked)
        {
            this.SupplyChainId = id;
            this.NextSequence = sequence;
            this.Locked = locked;
        }

        public string SupplyChainId { get; set; }

        public int NextSequence { get; set; }

        public bool Locked { get; set; }
    }
}
