﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
//using Castle.Core.Internal;

namespace DSAMailMarkConsole.ControlFiles
{
    /// <summary>
    /// Use the static function GetNextSupplyChainSequence
    ///
    /// This will do the following
    ///  1. Call Open(filename) until the SupplyChainSequences object returned has Status == SupplyChainSequenceStatus.Open
    ///  2. Call Update() with a new List of SupplyChainSequence with updated or added values. An entry can be removed although this is probably not desirable
    ///  3. Call Close() to close the control text file and permit other processes to access the file
    /// </summary>
    /// <remarks>
    /// If -1 is returned from GetNextSupplyChainSequence the call failed and processing should not continue
    /// </remarks>
    public class SupplyChainSequences
    {
        /// <summary>
        /// Section header for the Supply Chain Id .ctrl file
        /// </summary>
        private const string ControlHeader = "[Mailmark Supply Chain IDs]";

        /// <summary>
        /// Result of attempt to open the .ctrl file
        /// </summary>
        public enum SupplyChainSequenceStatus
        {
            Close,

            Open,

            FileInUse
        }

        /// <summary>
        /// Appended to the end of a supply chain between a sort getting the sequence and updating the sequence ready for the next sort.
        /// So if the supply chain Id is followed by * other processes know they need to wait, and try again, to get the next valid sequence
        /// </summary>
        private const char LockChar = '*';

        /// <summary>
        /// Appended to the end of a supply chain Id when the sequence is ready to be used by a sort
        /// </summary>
        private const char FreeChar = '-';

        /// <summary>
        /// Adds a line from a Supply Chain Ini File in format Id=Sequence[lockByte] to chains
        /// </summary>
        /// <param name="line">Text line from the Supply Chain Sequence Ini file</param>
        /// <result>The SupplyChainSequence extracted from the ini file line</result>
        private static SupplyChainSequence ExtractSupplyChainSequenceEntry(string line)
        {
            var splitLine = line.Split('=');

            if (splitLine.Length != 2)
                throw new Exception($"Control file line {line} not in correct format id=Integer{FreeChar} or id-integer{LockChar}");

            if (splitLine[1].Length < 2)
                throw new Exception($"Control file line {line} not in correct format id=Integer{FreeChar} or id-integer{LockChar}");

            var lockChar = splitLine[1][splitLine[1].Length - 1];

            if (lockChar != FreeChar && lockChar != LockChar)
                throw new Exception($"Control file line {line} not in correct format - last character must be {FreeChar} or {LockChar}");

            var id = splitLine[0];
            int sequence;
            var sequenceStr = "";

            try
            {
                sequenceStr = splitLine[1].Substring(0, splitLine[1].Length - 1);
                sequence = Convert.ToInt32(sequenceStr);
            }
            catch
            {
                throw new Exception(
                    $"Control file line {line} not in correct format - {sequenceStr} is not a sequence number");
            }

            switch (lockChar)
            {
                case FreeChar:
                    return new SupplyChainSequence(id, sequence, false);
                case LockChar:
                    return new SupplyChainSequence(id, sequence, true);
                default:
                    throw new Exception($"Invalid SupplyChain sequence entry {line} - Lock Char should be {FreeChar} or {LockChar}");
            }
        }

        private SupplyChainSequenceStatus Status { get; set; }

        /// <summary>
        /// Returns a newly created SupplyChainSequences object used to update the latest sequence numbers for SupplyChain Id's
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static SupplyChainSequences Open(string filename)
        {
            try
            {
                var chains = new List<SupplyChainSequence>();
                var stream = File.Open(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                using (var textReader = new StreamReader(stream, Encoding.UTF8, false, 1000, true))
                {
                    if (textReader.EndOfStream != true)
                    {
                        var headerLine = textReader.ReadLine();
                        if (headerLine != ControlHeader)
                            throw new Exception($"Control file missing valid section header - {ControlHeader} expected {headerLine} present");

                        string line;
                        while ((line = textReader.ReadLine()) != null)
                        {
                            if (!string.IsNullOrEmpty(line))
                                chains.Add(ExtractSupplyChainSequenceEntry(line));
                        }
                    }
                }

                var writer = new StreamWriter(stream);

                return new SupplyChainSequences(SupplyChainSequenceStatus.Open, stream, writer, chains);
            }
            catch (IOException e)
            {
                if (e.Message.Contains("used by another process"))
                    return new SupplyChainSequences(SupplyChainSequenceStatus.FileInUse);

                throw;
            }
        }

        private FileStream Stream { get; }

        private StreamWriter Writer { get; }

        /// <summary>
        /// Holds the latest sequence against each SupplyChain.
        /// Can be as read from text file or as amended - after call to Update.
        /// </summary>
        public List<SupplyChainSequence> Chains { get; private set; }

        /// <summary>
        /// Constructor used when Open fails
        /// </summary>
        /// <param name="status"></param>
        public SupplyChainSequences(SupplyChainSequenceStatus status)
        {
            Status = status;
        }

        /// <summary>
        /// Constructor used when Open succeeds
        /// </summary>
        /// <param name="status"></param>
        /// <param name="stream"></param>
        /// <param name="writer"></param>
        /// <param name="chains"></param>
        public SupplyChainSequences(SupplyChainSequenceStatus status, FileStream stream, StreamWriter writer, List<SupplyChainSequence> chains)
        {
            Stream = stream;
            Writer = writer;
            Chains = chains;
            Status = status;
        }

        /// <summary>
        /// Closes the stream and writer so other processes can access the control text file
        /// </summary>
        private void Close()
        {
            if (Status != SupplyChainSequenceStatus.Open)
                return;

            Writer.Close();
            Stream.Close();
            Chains.Clear();
            Status = SupplyChainSequenceStatus.Close;
        }

        /// <summary>
        /// Call to update the SupplyChain sequence details
        /// Writes to the control text file as well as updating the Chains object
        /// </summary>
        /// <remarks>
        /// Still needs to call Close() after this method in order to permit other processes to open the control text file
        /// </remarks>
        /// <param name="chains"></param>
        private void Update(List<SupplyChainSequence> chains)
        {
            Stream.Position = 0;
            Stream.SetLength(0);

            Writer.WriteLine(ControlHeader);
            foreach (var entry in chains)
            {
                var lockByte = entry.Locked == true ? LockChar : FreeChar;
                Writer.WriteLine(entry.SupplyChainId + "=" + entry.NextSequence.ToString("D8") + lockByte);
            }

            Writer.Flush();
            Chains = chains;
        }

        /// <summary>
        /// One call to GetNextSupplyChainSequence must complete before the next call
        /// </summary>
        private static readonly object Lock = new object();

        private const int MaximumSupplyChainSequence = 99999999;

        /// <summary>
        /// Method returns the next supply chain sequence number for the supplied supplyChainId.
        /// If the call fails eg due to corrupt control file an exception is raised and and processing will need manual intervention.
        /// </summary>
        /// <param name="filename">Text file that contains the supply chain sequences</param>
        /// <param name="supplyChainId">The supply chain Id to get next sequence for</param>
        /// <param name="advanceByRecords">How many records to advance the stored sequence ready for next call</param>
        /// <param name="milliSecondsBetweenAttempts">How many milliseconds to wait between attempts to get the segment code</param>
        /// <param name="attempts">How many attempts to get the segment code</param>
        /// <param name="statusMessage">Pass progress messages back to calling application</param>
        /// <param name="errorMessage">Pass error messages back to calling application</param>
        /// <returns></returns>
        public static int GetNextSupplyChainSequence(string filename, string supplyChainId, int advanceByRecords, int milliSecondsBetweenAttempts, int attempts, MessageCallbacks.MessageCallBack statusMessage = null, MessageCallbacks.ErrorMessageCallBack errorMessage = null)
        {
            lock (Lock)
            {
                try
                {
                    SupplyChainSequences sequences = null;
                    try
                    {
                        var counter = 0;
                        statusMessage($"Attempt {counter + 1} of {attempts} to get starting barcode sequence for SupplyChainId {supplyChainId}");
                        while ((sequences = Open(filename))?.Status != SupplyChainSequenceStatus.Open)
                        {
                            counter += 1;
                            Thread.Sleep(milliSecondsBetweenAttempts);
                            if (counter == attempts)
                                break;
                            statusMessage?.Invoke($"Attempt {counter + 1} of {attempts} to get starting barcode sequence for SupplyChainId {supplyChainId}");
                        }

                        if (sequences?.Status == SupplyChainSequenceStatus.Open)
                        {
                            var chain = sequences.Chains.FirstOrDefault(entry => entry.SupplyChainId == supplyChainId);
                            if (chain == null)
                            {
                                // no entry for the supply chain so add new entry and the return value will be 1
                                sequences.Chains.Add(new SupplyChainSequence(supplyChainId, 1 + advanceByRecords, false));
                                sequences.Update(sequences.Chains);
                                statusMessage?.Invoke($"Got starting barcode sequence 1 for SupplyChainId {supplyChainId}");
                                statusMessage?.Invoke($"Set starting barcode sequence to {1 + advanceByRecords} for next sort using SupplyChainId {supplyChainId}");
                                return 1;
                            }

                            if (chain.Locked)
                            {
                                var message = $"Error getting starting barcode sequence for SupplyChainId {supplyChainId} - sequence locked";
                                errorMessage?.Invoke(message, null);
                                throw new Exception(message);
                            }

                            var returnSequence = chain.NextSequence;

                            // SupplyChain sequence cannot go above 99999999 so wrap around to 00000001 when we would go above 99999999
                            if (chain.NextSequence + advanceByRecords > MaximumSupplyChainSequence)
                                chain.NextSequence = chain.NextSequence + advanceByRecords - MaximumSupplyChainSequence;
                            else
                                chain.NextSequence += advanceByRecords;

                            sequences.Update(sequences.Chains);
                            statusMessage?.Invoke($"Got next starting barcode sequence {returnSequence} for SupplyChainId {supplyChainId}");
                            statusMessage?.Invoke($"Set starting barcode sequence to {chain.NextSequence} for next sort using SupplyChainId {supplyChainId}");
                            return returnSequence;
                        }
                        else if (sequences?.Status == SupplyChainSequenceStatus.FileInUse)
                        {
                            var message = $"Error getting starting barcode sequence for SupplyChainId {supplyChainId} - control file in use";
                            errorMessage?.Invoke(message, null);
                            throw new Exception(message);
                        }
                        else if (sequences != null)
                        {
                            var message = $"Error getting starting barcode sequence for SupplyChainId {supplyChainId} - control file in use";
                            errorMessage?.Invoke(message, null);
                            throw new Exception(message);
                        }
                        else
                        {
                            var message = $"Unexpected error getting starting barcode sequence for SupplyChainId {supplyChainId}";
                            errorMessage?.Invoke(message, null);
                            throw new Exception(message);
                        }
                    }
                    finally
                    {
                        sequences?.Close();
                    }
                }
                catch (Exception exception)
                {
                    // Need to log the unexpected error - a different FileIO Exception to in use by another process?
                    var message = $"Exception {exception.Message} getting starting barcode sequence for SupplyChainId {supplyChainId}";
                    errorMessage?.Invoke(message, exception);
                    throw new Exception(message);
                }
            }
        }
    }
}
