﻿using System;
using System.IO;

namespace DSAMailMarkConsole.ControlFiles
{
    public class ExceptionFileInUse : IOException
    {
        public ExceptionFileInUse()
        {
        }

        public ExceptionFileInUse(string message)
            : base(message)
        {
        }

        public ExceptionFileInUse(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
