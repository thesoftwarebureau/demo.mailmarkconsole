﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAMailMarkConsole.ControlFiles
{
    public class MessageCallbacks
    {
        public delegate void MessageCallBack(string text);

        public delegate void ErrorMessageCallBack(string text, Exception exception);
    }
}
