﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace DSAMailMarkConsole.ControlFiles
{
    /// <summary>
    /// Open a WhistlSegmentCode control file, increment Cygnus and/or Swift Segment code(s), and update the control file
    /// Cygnus convention is that the control file is named SegmentControl.tnt and is in ProgramData\Swift\Sort\Resources\Control directory
    /// </summary>
    /// <remarks>
    /// Intended use scenario
    ///  1. Call Open(filename) until the WhistlSegmentCode object returned has Status == SegmentCodeStatus.Open
    ///  2. Call IncrementCygnusSegmentCode() for Cygnus or most likely IncrementSwiftSegmentCode for SwiftSort to move forward to the next SegmentCode
    ///  3. Call Update() to save the current CygnusSegmentCode and SwiftSegmentCode to disk
    ///  4. Call Close() to close the control text file and permit other processes to access the file
    /// </remarks>
    public class WhistlSegmentCode
    {
        public enum SegmentCodeStatus
        {
            Close,

            Open,

            FileInUse
        }

        private FileStream Stream { get; }

        private StreamWriter Writer { get; }

        private SegmentCodeStatus Status { get; set; }

        /// <summary>
        /// First Cygnus segment code character - can be A..H or 0..9
        /// </summary>
        private char CygnusSegmentCode1 { get; set; }

        /// <summary>
        /// Second Cygnus segment code character - can be A..Z or 0..9
        /// </summary>
        private char CygnusSegmentCode2 { get; set; }

        /// <summary>
        /// First Swift segment code character - can be I..Z
        /// </summary>
        private char SwiftSegmentCode1 { get; set; }

        /// <summary>
        /// Second Swift segment code character - can be A..Z or 0..9
        /// </summary>
        private char SwiftSegmentCode2 { get; set; }

        /// <summary>
        /// Returns the segment codes as a string as would be seen in the segment code file
        /// </summary>
        private string CodesAsString => new string(new [] {CygnusSegmentCode1, CygnusSegmentCode2, SwiftSegmentCode1, SwiftSegmentCode2});

        /// <summary>
        /// constructor used when the segment code file is open OK
        /// </summary>
        /// <param name="status"></param>
        /// <param name="stream"></param>
        /// <param name="writer"></param>
        /// <param name="codes"></param>
        public WhistlSegmentCode(SegmentCodeStatus status, FileStream stream, StreamWriter writer, string codes)
        {
            this.Status = status;
            Stream = stream;
            Writer = writer;

            if (codes.Length > 0)
                CygnusSegmentCode1 = codes[0];

            if (codes.Length > 1)
                CygnusSegmentCode2 = codes[1];

            if (codes.Length > 2)
                SwiftSegmentCode1 = codes[2];

            if (codes.Length > 3)
                SwiftSegmentCode2 = codes[3];
        }

        /// <summary>
        /// constructor used when the segment code file is not open OK
        /// </summary>
        /// <param name="fileInUse"></param>
        public WhistlSegmentCode(SegmentCodeStatus fileInUse)
        {
            Status = fileInUse;
        }

        /// <summary>
        /// The starting segment code values for Cygnus (first two chars) and Swift (second two chars)
        /// </summary>
        private const string StartingSegmentCode = "00I0";

        /// <summary>
        /// The Letters valid as first character for the Cygnus Segment Code - numeric/digits are not valid here
        /// </summary>
        private static readonly char[] SwiftValidChar1 = { 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

        /// <summary>
        /// The Letters valid as first character for the Swift Segment Code - numeric/digits 0..9 are also valid
        /// </summary>
        private static readonly char[] CygnusValidChar1 = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };

        /// <summary>
        /// Is the character in the range I..Z - characters permitted as first character for Swift Segment Code
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        private static bool CharIn_IZ_Range(char character)
        {
            for (var index = SwiftValidChar1.GetLowerBound(0); index <= SwiftValidChar1.GetUpperBound(0); index++)
            {
                if (character.Equals(SwiftValidChar1[index]))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Is the character in the range A..H - characters permitted as first character for Cygnus Segment Code
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        private static bool CharIn_AH_Range(char character)
        {
            for (var index = CygnusValidChar1.GetLowerBound(0); index <= CygnusValidChar1.GetUpperBound(0); index++)
            {
                if (character.Equals(CygnusValidChar1[index]))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Is the character in the range A..Z or 0..9
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        private static bool CharIsLetterOrDigit(char character)
        {
            return char.IsDigit(character) || CharIn_AH_Range(character) || CharIn_IZ_Range(character);
        }

        /// <summary>
        /// Is the Swift Character valid
        /// </summary>
        /// <param name="character">The character to test</param>
        /// <param name="position">Whether the character is the first (position 0) or second (position 1) character</param>
        /// <returns></returns>
        private static bool ValidSwiftCharacter(char character, int position)
        {
            switch (position)
            {                
                case 0:
                    return CharIn_IZ_Range(character);              // Validate the first character                
                case 1:
                    return CharIsLetterOrDigit(character);          // Validate the second character
                default:
                    throw new Exception($"position {position} passed into ValidSwiftCharacter not valid");
            }
        }

        /// <summary>
        /// Is the Cygnus Character valid
        /// </summary>
        /// <param name="character">The character to test</param>
        /// <param name="position">Whether the character is the first (position 0) or second (position 1) character</param>
        /// <returns></returns>
        private static bool ValidCygnusCharacter(char character, int position)
        {
            switch (position)
            {
                case 0:
                    return char.IsDigit(character) || CharIn_AH_Range(character);   // Validate the first character                
                case 1:
                    return CharIsLetterOrDigit(character);                          // Validate the second character
                default:
                    throw new Exception($"position {position} passed into ValidCygnusCharacter not valid");
            }
        }

        /// <summary>
        /// Returns a newly created MailmarkSequence object used to update the latest sequence numbers for SupplyChain Id's
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static WhistlSegmentCode Open(string filename)
        {
            try
            {
                var stream = File.Open(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                using (var textReader = new StreamReader(stream, Encoding.UTF8, false, 1000, true))
                {
                    var firstLine = textReader.ReadLine();
                    var writer = new StreamWriter(stream);

                    // Nothing previously written to the file so initialise with starting Cygnus and Swift segment codes
                    if (firstLine == null)
                    {
                        return new WhistlSegmentCode(SegmentCodeStatus.Open, stream, writer, StartingSegmentCode);
                    }

                    // Verify the first line of the segment code file correctly contains 4 characters
                    if (firstLine.Length != 4)
                        throw new Exception($"Segment Control file invalid >{firstLine}<");

                    // Verify each character is valid
                    if (!ValidCygnusCharacter(firstLine[0], 0))
                        throw new Exception($"Segment Control file invalid - First Character for CygnusSort >{firstLine}<");

                    if (!ValidCygnusCharacter(firstLine[1], 1))
                        throw new Exception($"Segment Control file invalid - Second Character for CygnusSort >{firstLine}<");

                    if (!ValidSwiftCharacter(firstLine[2], 0))
                        throw new Exception($"Segment Control file invalid - First Character for SwiftSort >{firstLine}<");

                    if (!ValidSwiftCharacter(firstLine[3], 1))
                        throw new Exception($"Segment Control file invalid - Second Character for SwiftSort >{firstLine}<");

                    return new WhistlSegmentCode(SegmentCodeStatus.Open, stream, writer, firstLine);
                }
            }
            catch (IOException e)
            {
                if (e.Message.Contains("used by another process"))
                    return new WhistlSegmentCode(SegmentCodeStatus.FileInUse);

                throw;
            }
        }

        /// <summary>
        /// Used to increment the second segment character for both Cygnus and Swift
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        private static char NextSegmentCharacterSecondPosition(char character)
        {
            switch (character)
            {
                case '0': return '1';
                case '1': return '2';
                case '2': return '3';
                case '3': return '4';
                case '4': return '5';
                case '5': return '6';
                case '6': return '7';
                case '7': return '8';
                case '8': return '9';
                case '9': return 'A';
                case 'A': return 'B';
                case 'B': return 'C';
                case 'C': return 'D';
                case 'D': return 'E';
                case 'E': return 'F';
                case 'F': return 'G';
                case 'G': return 'H';
                case 'H': return '0';
                default:
                    throw new Exception($"Character {character} passed into IncrementSegmentCodePosition1 not valid");
            }
        }

        /// <summary>
        /// Used to Increment a Cygnus Segment Code character
        /// </summary>
        /// <param name="character">The character to increment</param>
        /// <param name="position">Either 0 for first position or 1 for second position</param>
        /// <returns></returns>
        private static char NextCygnusSegmentCharacter(char character, int position)
        {
            switch (position)
            {
                case 0:
                    switch (character)
                    {
                        case '0': return '1';
                        case '1': return '2';
                        case '2': return '3';
                        case '3': return '4';
                        case '4': return '5';
                        case '5': return '6';
                        case '6': return '7';
                        case '7': return '8';
                        case '8': return '9';
                        case '9': return 'A';
                        case 'A': return 'B';
                        case 'B': return 'C';
                        case 'C': return 'D';
                        case 'D': return 'E';
                        case 'E': return 'F';
                        case 'F': return 'G';
                        case 'G': return 'H';
                        case 'H': return '0';
                        default:
                            throw new Exception(
                                $"Character {character} passed into IncrementCygnusSegmentCode not valid for position {position}");
                    }
                case 1:
                    return NextSegmentCharacterSecondPosition(character);
                default:
                    throw new Exception($"position {position} passed into IncrementCygnusSegmentCode not valid");
            }
        }

        /// <summary>
        /// Used to Increment a Swift Segment Code character
        /// </summary>
        /// <param name="character">The character to increment</param>
        /// <param name="position">Either 0 for first position or 1 for second position</param>
        /// <returns></returns>
        private static char NextSwiftSegmentCharacter(char character, int position)
        {
            switch (position)
            {
                case 0:
                    switch (character)
                    {
                        case 'I': return 'J';
                        case 'J': return 'K';
                        case 'K': return 'L';
                        case 'L': return 'M';
                        case 'M': return 'N';
                        case 'N': return 'O';
                        case 'O': return 'P';
                        case 'P': return 'Q';
                        case 'Q': return 'R';
                        case 'R': return 'S';
                        case 'S': return 'T';
                        case 'T': return 'U';
                        case 'U': return 'V';
                        case 'V': return 'W';
                        case 'W': return 'X';
                        case 'X': return 'Y';
                        case 'Y': return 'Z';
                        case 'Z': return 'I';
                        default:
                            throw new Exception($"Character {character} passed into IncrementSwiftSegmentCode not valid for position {position}");
                    }
                case 1:
                    return NextSegmentCharacterSecondPosition(character);
                default:
                    throw new Exception($"position {position} passed into IncrementSwiftSegmentCode not valid");
            }
        }

        /// <summary>
        /// Increments the Cygnus Segment Code
        /// </summary>
        private void IncrementCygnusSegmentCode()
        {
            CygnusSegmentCode2 = NextCygnusSegmentCharacter(CygnusSegmentCode2, 1);

            if (CygnusSegmentCode2 == '0')
                CygnusSegmentCode1 = NextCygnusSegmentCharacter(CygnusSegmentCode1, 0);
        }

        /// <summary>
        /// Increments the Swift Segment Code
        /// </summary>
        private void IncrementSwiftSegmentCode()
        {
            SwiftSegmentCode2 = NextSwiftSegmentCharacter(SwiftSegmentCode2, 1);

            if (SwiftSegmentCode2 == '0')
                SwiftSegmentCode1 = NextSwiftSegmentCharacter(SwiftSegmentCode1, 0);
        }

        /// <summary>
        /// Close the file writer so that another process can access the underlying file and reset the Segment codes to "starting" values
        /// </summary>
        private void Close()
        {
            if (Status != SegmentCodeStatus.Open)
                return;

            Writer.Close();
            Stream.Close();
            CygnusSegmentCode1 = StartingSegmentCode[0];
            CygnusSegmentCode2 = StartingSegmentCode[1];
            SwiftSegmentCode1 = StartingSegmentCode[2];
            SwiftSegmentCode2 = StartingSegmentCode[3];
            Status = SegmentCodeStatus.Close;
        }

        /// <summary>
        /// Writes the current Segment Code values back to the underlying file
        /// </summary>
        private void Update()
        {
            Stream.Position = 0;
            Stream.SetLength(0);

            Writer.WriteLine(CodesAsString);
            Writer.Flush();
        }

        /// <summary>
        /// One call to GetNextSegmentCode must complete before the next call
        /// </summary>
        private static readonly object Lock = new object();

        /// <summary>
        /// 1. Loads the latest segment codes from filename, 2. Increments the segment code, 3. Saves the text file, 4. Returns the segment code
        /// </summary>
        /// <param name="filename">Text file that contains the segment code</param>
        /// <param name="milliSecondsBetweenAttempts">How many milliseconds to wait between attempts to get the segment code</param>
        /// <param name="attempts">How many attempts to get the segment code</param>
        /// <param name="statusMessage">Pass progress messages back to calling application</param>
        /// <param name="errorMessage">Pass error messages back to calling application</param>
        /// <returns>The Segment code to use</returns>
        public static string GetNextSegmentCode(string filename, int milliSecondsBetweenAttempts, int attempts, MessageCallbacks.MessageCallBack statusMessage = null, MessageCallbacks.ErrorMessageCallBack errorMessage = null)
        {
            lock (Lock)
            {
                try
                {
                    WhistlSegmentCode segmentCode = null;
                    try
                    {
                        var counter = 0;
                        statusMessage?.Invoke($"Attempt {counter + 1} of {attempts} to get next Whistl Segment Code");
                        while ((segmentCode = Open(filename))?.Status != SegmentCodeStatus.Open)
                        {
                            counter += 1;
                            Thread.Sleep(milliSecondsBetweenAttempts);
                            if (counter == attempts)
                                break;
                            statusMessage?.Invoke($"Attempt {counter + 1} of {attempts} to get next Whistl Segment Code");
                        }

                        if (segmentCode?.Status == SegmentCodeStatus.Open)
                        {
                            segmentCode.IncrementSwiftSegmentCode();
                            var result = new string(new[] { segmentCode.SwiftSegmentCode1, segmentCode.SwiftSegmentCode2 });
                            segmentCode.Update();
                            statusMessage?.Invoke($"Got next Whistl Segment Code {result}");
                            return result;
                        }
                        else if (segmentCode?.Status == SegmentCodeStatus.FileInUse)
                        {
                            errorMessage?.Invoke($"Error getting Whistl Segment Code control file {filename} - segment control file in use", null);
                            return null;
                        }
                        else if (segmentCode != null)
                        {
                            errorMessage?.Invoke($"Unexpected Status {segmentCode.Status} getting Whistl Segment Code", null);
                            return null;
                        }
                        else
                        {
                            errorMessage?.Invoke($"Unexpected error getting Whistl Segment Code", null);
                            return null;
                        }
                    }
                    finally
                    {
                        segmentCode?.Close();
                    }
                }
                catch (Exception exception)
                {
                    // Need to log the unexpected error - a different FileIO Exception to in use by another process?
                    errorMessage?.Invoke($"Exception {exception.Message} getting Whistl Segment Code", exception);
                    return null;
                }
            }
        }
    }
}
