﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using DSAMailMarkConsole.ControlFiles;
using Fonet;
using SoftwareBureau.LimeLicence.TA;
using SoftwareBureau.UniversalSortEngine;
using SoftwareBureau.UniversalSortEngine.MailSort;

namespace DSAMailMarkConsole
{
    // Demo application that performs a DSA MailMark sort
    // Carriers supported are Royal Mail, Whistl, Citipost, SecuredMail, UKMail
    // The user is prompted to enter the carrier at the command prompt at the start of the program 
    // The API user must ensure the parameters are consistent with carrier rules
    class Program
    {
        /// <summary>
        /// Number attempts to get Supply Chain Starting Sequence and/or Whistl Segment Code
        /// </summary>
        private const int Attempts = 5;

        /// <summary>
        /// Milliseconds to wait between attempts to get Supply Chain Starting Sequence and/or Whistl Segment Code
        /// </summary>
        private const int AttemptWait = 3;

        /// <summary>
        /// Path to sort bin files
        /// </summary>
        private const string ResourcePath = @"..\..\db\RMSort.bin";

        /// <summary>
        /// Supply Chain Control File - for default Swift install file is C:\ProgramData\SoftwareBureau\Swift\Sort\Resources\Control\MailmarkSupplyChainIDSequence.ctl
        /// </summary>
        private const string SupplyChainControlFile = @"..\..\Control\MailmarkSupplyChainIDSequence.ctl";

        /// <summary>
        /// Whistl Segment Control File - for default Swift install file is C:\ProgramData\SoftwareBureau\Swift\Sort\Resources\Control\SegmentControl.tnt
        /// </summary>
        private const string WhistlSegmentControlFile = @"..\..\Control\SegmentControl.tnt";

        /// <summary>
        /// Location of output files after processing
        /// </summary>
        private const string OutputFolder = @"..\..\TestData\";

        /// <summary>
        /// All output files based on this name
        /// </summary>
        private const string Output = "Output";

        /// <summary>
        /// Path to location of XSL files that power FONet production of Planning Analysis PDF report
        /// </summary>
        private const string XslPath = @"..\..\XSL\usesort-fo.xsl";

        /// <summary>
        /// Path to location of XSL file that outputs bag breakdown listing for UnSorted sorts
        /// </summary>
        private const string BagBreakdownXslPath = @"..\..\XSL\BagBreakdown.xsl";

        internal class BaseSortArgs
        {
            public string InputFile { get; set; }

            public string OutputFile { get; set; }

            public USEParams Params { get; internal set; }
        }

        /// <summary>
        /// Arguments for a Unsorted IO sort - different Sort object necessary for UnSortedArgs cf MailSortArgs
        /// </summary>
        internal class UnSortedArgs : BaseSortArgs
        {
            public IUSEUnsorted Sort { get; set; }
        }

        /// <summary>
        /// Arguments for a file IO sort
        /// </summary>
        internal class MailSortArgs : BaseSortArgs
        {
            public IUSESort Sort { get; set; }

            public bool SimpleBag { get; internal set; }
        }

        internal class MailSortCounts
        {
            internal int MailMarkValid { get; set; }

            internal int ZonalCount { get; set; }

            internal int NationalCount { get; set; }
        }

        /// <summary>
        /// Provides feedback to the user on what the sort is doing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void UseSort_StatusUpdate(object sender, string e)
        {
            Console.WriteLine(e);
        }

        /// <summary>
        /// User enters required parameters at the command prompt
        /// </summary>
        /// <returns></returns>
        private static ConsoleArgs GetUserDefinedParameters()
        {
            var args = new ConsoleArgs();

            // get the carrier from the user
            Console.WriteLine($"enter carrier - valid values are {USEParams.Carriers.RoyalMail} {USEParams.Carriers.Whistl}, {USEParams.Carriers.UKMail}, {USEParams.Carriers.Citipost}, {USEParams.Carriers.SecuredMail}");
            var carrierString = Console.ReadLine();
            if (!Enum.TryParse(carrierString, true, out USEParams.Carriers carrier))
            {
                Console.WriteLine($"carrier {carrierString} invalid - any key to exit");
                Console.ReadKey();
                return null;
            }
            args.Carrier = carrier;

            // validate the carrier
            if (carrier != USEParams.Carriers.RoyalMail && carrier != USEParams.Carriers.Citipost && carrier != USEParams.Carriers.SecuredMail && carrier != USEParams.Carriers.UKMail && carrier != USEParams.Carriers.Whistl)
            {
                Console.WriteLine($"carrier {carrier} not supported - any key to exit");
                Console.ReadKey();
                return null;
            }

            // if Royal Mail no form of Zonal sort is permitted
            if (carrier == USEParams.Carriers.RoyalMail)
                args.SplitType = USEDSAParams.DSASplitTypes.National;

            // but for other carriers get the SplitType from the user
            else if (carrier == USEParams.Carriers.UKMail || carrier == USEParams.Carriers.Citipost || carrier == USEParams.Carriers.OnePost)
            {
                // for UKMail, Citipost and OnePost National, Zonal, and Split are all permitted
                Console.WriteLine($"Enter split type - valid values are {USEDSAParams.DSASplitTypes.National}, {USEDSAParams.DSASplitTypes.Zonal}, {USEDSAParams.DSASplitTypes.Arbitrage}");
                var splitString = Console.ReadLine();
                if (!Enum.TryParse(splitString, true, out USEDSAParams.DSASplitTypes splitType))
                {
                    Console.WriteLine($"SplitType {splitString} invalid - any key to exit");
                    Console.ReadKey();
                    return null;
                }

                args.SplitType = splitType;
            }
            else
            {
                // if Whistl or SecuredMail only National or Zonal supported (no "Split" sorts)
                Console.WriteLine($"Enter split type - valid values are {USEDSAParams.DSASplitTypes.National}, {USEDSAParams.DSASplitTypes.Zonal}");
                var splitString = Console.ReadLine();
                if (!Enum.TryParse(splitString, true, out USEDSAParams.DSASplitTypes splitType))
                {
                    Console.WriteLine($"SplitType {splitString} invalid - any key to exit");
                    Console.ReadKey();
                    return null;
                }

                // validate the split type is not Split
                if (splitType == USEDSAParams.DSASplitTypes.Arbitrage)
                {
                    Console.WriteLine($"SplitType {splitString} invalid for {args.Carrier} - any key to exit");
                    Console.ReadKey();
                    return null;
                }

                args.SplitType = splitType;
            }

            if (args.SplitType == USEDSAParams.DSASplitTypes.National || args.SplitType == USEDSAParams.DSASplitTypes.Arbitrage)
            {
                Console.WriteLine("enter National Supply Chain Id");
                var nationalSupplyChainId = Console.ReadLine();
                if (string.IsNullOrEmpty(nationalSupplyChainId))
                {
                    Console.WriteLine("National Supply Chain Id not specified - any key to exit");
                    Console.ReadKey();
                    return null;
                }
                args.NationalSupplyChainId = nationalSupplyChainId;
            }

            if (args.SplitType == USEDSAParams.DSASplitTypes.Zonal || args.SplitType == USEDSAParams.DSASplitTypes.Arbitrage)
            {
                Console.WriteLine("enter Zonal Supply Chain Id");
                var zonalSupplyChainId = Console.ReadLine();
                if (string.IsNullOrEmpty(zonalSupplyChainId))
                {
                    Console.WriteLine("Zonal Supply Chain Id not specified - any key to exit");
                    Console.ReadKey();
                    return null;
                }
                args.ZonalSupplyChainId = zonalSupplyChainId;
            }

            return args;
        }

        /// <summary>
        /// Check the scid is specified and 6-7 characters long
        /// </summary>
        /// <param name="scid">scid to verify</param>
        /// <param name="scidType">National or Zonal caption for error message</param>
        /// <returns></returns>
        private static bool VerifySCID(string scid, string scidType)
        {
            if (string.IsNullOrEmpty(scid))
            {
                Console.WriteLine($"{scidType} not specified");
                return false;
            }

            if (scid.Length < 6 || scid.Length > 7)
            {
                Console.WriteLine($"{scidType} {scid} must be 6-7 characters long");
                return false;
            }

            return true;
        }

        private const string nationalSupplyChainToken = "/N=";

        /// <summary>
        /// Gets sort parameters from command line parameters .... little validation
        /// </summary>
        /// <returns></returns>
        private static ConsoleArgs ParseCommandLineParameters(string[] userArgs)
        {
            const string carrierToken = "/CARRIER=";
            const string zonalSupplyChainToken = "/Z=";
            const string splitTypeToken = "/SPLIT=";
            const string unsortedToken = "/UNSORTED=";
            const string serviceLevelToken = "/SERVICELEVEL=";
            const string pieceToken = "/PIECE=";
            const string containerToken = "/CONTAINER=";

            const string mailTypeToken = "/MAILTYPE=";
            const string sortTypeToken = "/SORTTYPE=";
            const string sustainToken = "/SUS=";
            const string mailDescToken = "/TEST=";
            const string sort48Token = "/SORT48=";
			
            const string weightToken = "/WEIGHT=";			
            const string maxContainerContentWeightToken = "/MAXCONTAINERCONTENTWEIGHT=";
            
			const string inputFileToken = "/FILE=";			
            const string minimumSelectionSizeToken = "/MINIMUMSELECTIONSIZE=";

            //const string outdirToken = "/OUT=";
            const string directResidueOrderToken = "/ORDER=";			
			const string allowSelectionTrayWithdrawalToken = "/ALLOWSELECTIONTRAYWITHDRAWAL=";
            const string allowAutoDpsLevelAdjustmentToken = "/ALLOWAUTODPSLEVELADJUSTMENT=";

            const string switchOffVolumeCheckingToken = "/SWITCHOFFVOLUMECHECKING=";
            const string simpleBagToken = "/SIMPLEBAG=";
            const string dsaMachinableToken = "/DSAMACHINABLE=";

            const string partialToken = "/PARTIAL=";

            // This should be the name of a field that contains a customer reference which should go in the Spare9 field of the gemma file
            const string customerRefToken = "/CUSTREF=";

            const string mailmarkBatchRefToken = "/BATCHREF=";

            var args = new ConsoleArgs();

            foreach (var arg in userArgs)
            {
                if (arg.Length > carrierToken.Length && arg.ToUpper().Substring(0, carrierToken.Length) == carrierToken)
                {
                    var param = arg.Substring(carrierToken.Length);
                    if (!Enum.TryParse(param, true, out USEParams.Carriers carrier))
                    {
                        Console.WriteLine($"carrier {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.Carrier = carrier;
                }
                else if (arg.Length > nationalSupplyChainToken.Length && arg.ToUpper().Substring(0, nationalSupplyChainToken.Length) == nationalSupplyChainToken)
                {
                    var param = arg.Substring(nationalSupplyChainToken.Length);
                    if (string.IsNullOrEmpty(param))
                    {
                        Console.WriteLine("Blank National Supply Chain Id specified - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.NationalSupplyChainId = param;
                }
                else if (arg.Length > zonalSupplyChainToken.Length && arg.ToUpper().Substring(0, zonalSupplyChainToken.Length) == zonalSupplyChainToken)
                {
                    var param = arg.Substring(zonalSupplyChainToken.Length);
                    if (string.IsNullOrEmpty(param))
                    {
                        Console.WriteLine("Blank Zonal Supply Chain Id specified - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.ZonalSupplyChainId = param;
                }
                else if (arg.Length > splitTypeToken.Length && arg.ToUpper().Substring(0, splitTypeToken.Length) == splitTypeToken)
                {
                    var param = arg.Substring(splitTypeToken.Length);
                    if (!Enum.TryParse(param, true, out USEDSAParams.DSASplitTypes splitType))
                    {
                        Console.WriteLine($"SplitType {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.SplitType = splitType;
                }
                else if (arg.Length > unsortedToken.Length && arg.ToUpper().Substring(0, unsortedToken.Length) == unsortedToken)
                {
                    var param = arg.Substring(unsortedToken.Length).ToUpper();

                    if (param[0] != 'T' && param[0] != 'F')
                    {
                        Console.WriteLine($"Withdrawal {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.Unsorted = param.Length > 0 && param[0] == 'T';
                }
                else if (arg.Length > serviceLevelToken.Length && arg.ToUpper().Substring(0, serviceLevelToken.Length) == serviceLevelToken)
                {
                    var param = arg.Substring(serviceLevelToken.Length);
                    if (!Enum.TryParse(param, true, out USEDSAParams.ServiceLevels serviceLevel))
                    {
                        Console.WriteLine($"ServiceLevel {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.ServiceLevel = serviceLevel;
                }
                else if (arg.Length > pieceToken.Length && arg.ToUpper().Substring(0, pieceToken.Length) == pieceToken)
                {
                    var param = arg.Substring(pieceToken.Length);
                    if (!Enum.TryParse(param, true, out USEDSAParams.Pieces piece))
                    {
                        Console.WriteLine($"Piece {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.Piece = piece;
                }
                else if (arg.Length > containerToken.Length && arg.ToUpper().Substring(0, containerToken.Length) == containerToken)
                {
                    var param = arg.Substring(containerToken.Length);

                    if (!Enum.TryParse(param, true, out USEDSAParams.Containers container))
                    {
                        Console.WriteLine($"Container {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.Container = container;
                }
                else if (arg.Length > maxContainerContentWeightToken.Length && arg.ToUpper().Substring(0, maxContainerContentWeightToken.Length) == maxContainerContentWeightToken)
                {
                    var param = arg.Substring(maxContainerContentWeightToken.Length).ToUpper();
                    try
                    {
                        args.MaxContainerContentWeight = Convert.ToInt32(param);
                    }
                    catch
                    {
                        Console.WriteLine($"MaxContainerContentWeight {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                }
                else if (arg.Length > weightToken.Length && arg.ToUpper().Substring(0, weightToken.Length) == weightToken)
                {
                    var param = arg.Substring(weightToken.Length).ToUpper();
                    try
                    {
                        args.ItemWeight = Convert.ToInt32(param);
                    }
                    catch
                    {
                        Console.WriteLine($"ItemWeight {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                }
                else if (arg.Length > mailTypeToken.Length && arg.ToUpper().Substring(0, mailTypeToken.Length) == mailTypeToken)
                {
                    var param = arg.Substring(mailTypeToken.Length);
                    // if (param == "")
                    if (!Enum.TryParse(param, true, out USEParams.MailTypes mailType))
                    {
                        Console.WriteLine($"MailType {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    //Console.WriteLine($"MT Enterd: {param}");
                    args.MailType = mailType;
                }
                else if (arg.Length > sortTypeToken.Length && arg.ToUpper().Substring(0, sortTypeToken.Length) == sortTypeToken)
                {
                    var param = arg.Substring(sortTypeToken.Length);
                    if (!Enum.TryParse(param, true, out USEParams.SortTypes sortType))
                    {
                        Console.WriteLine($"SortType {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    args.SortType = sortType;
                }
                else if (arg.Length > sustainToken.Length && arg.ToUpper().Substring(0, sustainToken.Length) == sustainToken)
                {
                    var param = arg.Substring(sustainToken.Length);
                    // if (param == "")
                    if (!Enum.TryParse(param, true, out USEParams.Sustainabilities Sustainabilities))
                    {
                        Console.WriteLine($"Sustain {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    args.Sustainabilities = Sustainabilities;
                }
                else if (arg.Length > mailDescToken.Length && arg.ToUpper().Substring(0, mailDescToken.Length) == mailDescToken)
                {
                    var param = arg.Substring(mailDescToken.Length);
                    if (string.IsNullOrEmpty(param))
                    {
                        Console.WriteLine($"Test No {param} blank - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.MailingDesc = param;
                }
                else if (arg.Length > sort48Token.Length && arg.ToUpper().Substring(0, sort48Token.Length) == sort48Token)
                {
                    var param = arg.Substring(sort48Token.Length).ToUpper();
                    if (param[0] != 'T' && param[0] != 'F')
                    {
                        Console.WriteLine($"Sort48 {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    args.Sort48 = param.Length > 0 && param[0] == 'T';
                }
                else if (arg.Length > inputFileToken.Length && arg.ToUpper().Substring(0, inputFileToken.Length) == inputFileToken)
                {
                    var param = arg.Substring(inputFileToken.Length);
                    if (string.IsNullOrEmpty(param))                    //if (!Enum.TryParse(param, true, out USEParams.MailTypes MailTypes))
                    {
                        Console.WriteLine($"inputfile {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    Console.WriteLine($"Input File: {param}");
                    args.InputFile = @"..\..\InputData\" + param;
                }
                else if (arg.Length > minimumSelectionSizeToken.Length && arg.ToUpper().Substring(0, minimumSelectionSizeToken.Length) == minimumSelectionSizeToken)
                {
                    var param = arg.Substring(minimumSelectionSizeToken.Length).ToUpper();
                    try
                    {
                        args.MinimumSelectionSize = Convert.ToInt32(param);
                    }
                    catch
                    {
                        Console.WriteLine($"MinimumSelectionSize {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                }
                else if (arg.Length > directResidueOrderToken.Length && arg.ToUpper().Substring(0, directResidueOrderToken.Length) == directResidueOrderToken)
                {
                    var param = arg.Substring(directResidueOrderToken.Length);
                    if (!Enum.TryParse(param, true, out USEParams.DirectResidueOrders DirectResidueOrders))
                    {
                        Console.WriteLine($"Output Order {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    args.DirectResidueOrder = DirectResidueOrders;
                    /*
                        AlternateDirectResidue = 0,
                        AlternateResidueDirect = 1,
                        SeparateDirectResidue = 2,
                        SeparateResidueDirect = 3    
                    */
                }
                else if (arg.Length > allowSelectionTrayWithdrawalToken.Length && arg.ToUpper().Substring(0, allowSelectionTrayWithdrawalToken.Length) == allowSelectionTrayWithdrawalToken)
                {
                    var param = arg.Substring(allowSelectionTrayWithdrawalToken.Length).ToUpper();

                    if (param[0] != 'T' && param[0] != 'F')
                    {
                        Console.WriteLine($"Withdrawal {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.AllowSelectionTrayWithdrawal = param.Length > 0 && param[0] == 'T';
                }
                else if (arg.Length > allowAutoDpsLevelAdjustmentToken.Length && arg.ToUpper().Substring(0, allowAutoDpsLevelAdjustmentToken.Length) == allowAutoDpsLevelAdjustmentToken)
                {
                    var param = arg.Substring(allowAutoDpsLevelAdjustmentToken.Length).ToUpper();

                    if (param[0] != 'T' && param[0] != 'F')
                    {
                        Console.WriteLine($"Withdrawal {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.AllowAutoDpsLevelAdjustment = param.Length > 0 && param[0] == 'T';
                }
                else if (arg.Length > switchOffVolumeCheckingToken.Length && arg.ToUpper().Substring(0, switchOffVolumeCheckingToken.Length) == switchOffVolumeCheckingToken)
                {
                    var param = arg.Substring(switchOffVolumeCheckingToken.Length).ToUpper();

                    if (param[0] != 'T' && param[0] != 'F')
                    {
                        Console.WriteLine($"Withdrawal {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }

                    args.SwitchOffVolumeChecking = param.Length > 0 && param[0] == 'T';
                }
                else if (arg.Length > simpleBagToken.Length && arg.ToUpper().Substring(0, simpleBagToken.Length) == simpleBagToken)
                {
                    var param = arg.Substring(simpleBagToken.Length);
                    if ((param.ToLower() != "t") && (param.ToLower() != "f"))
                    {
                        Console.WriteLine($"Bag File Format {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    else
                    if (param.ToLower() == "t") { args.SimpleBag = true; } else { args.SimpleBag = false; }
                }
                else if (arg.Length > dsaMachinableToken.Length && arg.ToUpper().Substring(0, dsaMachinableToken.Length) == dsaMachinableToken)
                {
                    var param = arg.Substring(dsaMachinableToken.Length);
                    if ((param.ToLower() != "t") && (param.ToLower() != "f"))
                    {
                        Console.WriteLine($"DSAMachinable {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    else
                    if (param.ToLower() == "t") { args.DSAMachinable = true; } else { args.DSAMachinable = false; }
                }
                else if (arg.Length > partialToken.Length && arg.ToUpper().Substring(0, partialToken.Length) == partialToken)
                {
                    var param = arg.Substring(partialToken.Length);
                    if ((param.ToLower() != "t") && (param.ToLower() != "f"))
                    {
                        Console.WriteLine($"Partial {param} invalid - any key to exit");
                        Console.ReadKey();
                        return null;
                    }
                    else
                    if (param.ToLower() == "t") { args.Partial = true; } else { args.Partial = false; }
                }
                else if (arg.Length > customerRefToken.Length && arg.ToUpper().Substring(0, customerRefToken.Length) == customerRefToken)
                {
                    var param = arg.Substring(customerRefToken.Length);
                    args.CustomerReferenceField = param;
                }
                else if (arg.Length > mailmarkBatchRefToken.Length && arg.ToUpper().Substring(0, mailmarkBatchRefToken.Length) == mailmarkBatchRefToken)
                {
                    var param = arg.Substring(mailmarkBatchRefToken.Length);
                    args.MailmarkBatchReference = param;
                }

                else throw new Exception($"unexpected parameter {arg}");
            }

            if (!string.IsNullOrEmpty(args.CustomerReferenceField) && args.SortType != USEParams.SortTypes.Mailmark)
            {
                Console.WriteLine($"Mailmark sort required for {customerRefToken}=");
                return null;
            }

            if (!string.IsNullOrEmpty(args.MailmarkBatchReference) && args.SortType != USEParams.SortTypes.Mailmark)
            {
                Console.WriteLine($"Mailmark sort required for {mailmarkBatchRefToken}=");
                return null;
            }

            if (string.IsNullOrEmpty(args.MailmarkBatchReference) && args.SortType == USEParams.SortTypes.Mailmark)
            {
                Console.WriteLine($"Mailmark sort requires {mailmarkBatchRefToken}=");
                return null;
            }

            // Some validation
            switch (args.Carrier)
            {
                case USEParams.Carriers.RoyalMail when args.SplitType != USEDSAParams.DSASplitTypes.National:
                    Console.WriteLine("Only National sorts permitted for Royal Mail carrier");
                    return null;
                case USEParams.Carriers.RoyalMail when args.MailMark && !VerifySCID(args.NationalSupplyChainId, "NationalSupplyChainId"):
                    return null;
                case USEParams.Carriers.RoyalMail:
                    break;
                case USEParams.Carriers.Whistl:
                case USEParams.Carriers.SecuredMail:
                    switch (args.SplitType)
                    {
                        case USEDSAParams.DSASplitTypes.Arbitrage:
                            Console.WriteLine("Arbitrage is not permitted for Whistl or Secured Mail");
                            return null;
                        case USEDSAParams.DSASplitTypes.National when args.MailMark && !VerifySCID(args.NationalSupplyChainId, "NationalSupplyChainId"):
                            return null;
                        case USEDSAParams.DSASplitTypes.Zonal when args.MailMark && !VerifySCID(args.ZonalSupplyChainId, "ZonalSupplyChainId"):
                            return null;
                    }
                    break;
                case USEParams.Carriers.Citipost:
                case USEParams.Carriers.UKMail:
                case USEParams.Carriers.OnePost:
                    if (args.SplitType == USEDSAParams.DSASplitTypes.Arbitrage || args.SplitType == USEDSAParams.DSASplitTypes.National )
                        if (args.MailMark && !VerifySCID(args.NationalSupplyChainId, "NationalSupplyChainId"))
                            return null;

                    if (args.SplitType == USEDSAParams.DSASplitTypes.Arbitrage || args.SplitType == USEDSAParams.DSASplitTypes.Zonal)
                        if (args.MailMark && !VerifySCID(args.ZonalSupplyChainId, "ZonalSupplyChainId"))
                        {
                            return null;
                        }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (args.Unsorted && args.ServiceLevel == USEParams.ServiceLevels.Economy)
            {
                Console.WriteLine("Economy service level not permitted with UnSorted");
                return null;
            }

            return args;
        }

        /// <summary>
        /// Control directory must exist so check and create if it does not exist
        /// </summary>
        /// <returns></returns>
        private static bool CreateControlDirectory()
        {
            if (Directory.Exists(@"..\..\control")) return true;

            try
            {
                Directory.CreateDirectory(@"..\..\control");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Unable to create control directory {e}");
                return false;
            }
        }

        static void Main(string[] args)
        {
            if (!CreateControlDirectory()) return;

            // either parses command prompt parameters or if no command prompt parameters specified
            // gets the user to enter carrier, SplitType, National and Zonal Supply Chain Ids via the command prompt
            var consoleArgs = args.Length == 0 ? GetUserDefinedParameters() : ParseCommandLineParameters(args);

            if (consoleArgs == null) return;

            if (consoleArgs.Unsorted)
            {
                UnSorted(consoleArgs);
                // pause and prompt user to exit
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
                return;
            }

            // set a default MaxContainerContentWeight if the user has not specifically set it
            if (consoleArgs.MaxContainerContentWeight == 0)
                consoleArgs.SetDefaultMaxContainerContentWeight();

            // create object to hold all parameters required to run a sort
            var fileMailSortArgs = new MailSortArgs
            {
                InputFile = string.IsNullOrEmpty(consoleArgs.InputFile) ? @"..\..\TestData\Input.psv" : consoleArgs.InputFile,
                OutputFile = Path.Combine(OutputFolder, $"{Output}.psv"),
                SimpleBag = consoleArgs.SimpleBag
            };

            Console.WriteLine($"user parameters - Test: {consoleArgs.MailingDesc }, {consoleArgs.Carrier}, {consoleArgs.InputFile}");
            Console.WriteLine($".............. -  {consoleArgs.SortType}, {consoleArgs.MailType}, {consoleArgs.Piece}, {consoleArgs.ServiceLevel}, {consoleArgs.Container} ");
            Console.WriteLine($".............. -  {consoleArgs.SplitType}, n: {consoleArgs.NationalSupplyChainId}, z: {consoleArgs.ZonalSupplyChainId}, weight: {consoleArgs.ItemWeight}, maxwgt: {consoleArgs.MaxContainerContentWeight}, minsel: {consoleArgs.MinimumSelectionSize}");
            Console.WriteLine($".............. -  SUS: {consoleArgs.Sustainabilities}, Sort48: {consoleArgs.Sort48}, unsorted: { consoleArgs.Unsorted}, Mailmark Customer Reference Field: {consoleArgs.CustomerReferenceField}");

            // Get the correct type of params object dependent upon carrier and whether we are performing a MailMark sort
            var useParams = consoleArgs.Carrier.GetParams(consoleArgs.SortType == USEParams.SortTypes.Mailmark);

            if (useParams is USEDSAParams || useParams is USEDSAMailmarkParams)
                Console.WriteLine($".............. -  DSA Machinable: {consoleArgs.DSAMachinable}");

            // Set the parameters with fixed values ... NB different sort objects require different combination of parameters
            consoleArgs.Carrier.SetParams(useParams, consoleArgs);

            if (useParams.Unsorted)
            {
                Console.WriteLine("Unsorted not expected - any key to exit");
                Console.ReadKey();
                return;
            }

            // Extra work for Whistl - need to set the Whistl Segment Code
            if (consoleArgs.Carrier == USEParams.Carriers.Whistl)
            {
                var segmentCode = WhistlSegmentCode.GetNextSegmentCode(WhistlSegmentControlFile, AttemptWait, Attempts, DisplaySupplyChainMessage, DisplaySupplyChainError);
                if (string.IsNullOrEmpty(segmentCode))
                {
                    Console.WriteLine("error getting next Whistl Segment Code - any key to exit");
                    Console.ReadKey();
                    return;
                }

                if (!(useParams is USEDSAMailmarkParams) && !(useParams is USEDSAParams)) throw new Exception("useParams expected to be USEDSAParams or USEDSAMailmarkParams");

                if (useParams is USEDSAMailmarkParams @params) 
                    @params.WhistlSegmentCode = segmentCode;
                else 
                    (useParams as USEDSAParams).WhistlSegmentCode = segmentCode;
            }

            // Set the supply chain Ids .......
            if (useParams is USEMailmarkParams mailMarkParams)
            {
                // The RoyalMail service is only National
                mailMarkParams.SCID = consoleArgs.NationalSupplyChainId;
            }
            // ...... complicated by supply chain Ids for DSAs being National and Zonal (note National and Zonal supply chain Ids look the same) 
            else if (useParams is USEDSAMailmarkParams useDsaMailMarkParams)
            {
                if (consoleArgs.SplitType == USEDSAParams.DSASplitTypes.National || consoleArgs.SplitType == USEDSAParams.DSASplitTypes.Arbitrage)
                    useDsaMailMarkParams.SCIDNational = consoleArgs.NationalSupplyChainId;

                if (consoleArgs.SplitType == USEDSAParams.DSASplitTypes.Zonal || consoleArgs.SplitType == USEDSAParams.DSASplitTypes.Arbitrage)
                    useDsaMailMarkParams.SCIDZonal = consoleArgs.ZonalSupplyChainId;
            }

            fileMailSortArgs.Params = useParams;

            // create sort object - Prices.bin and DSACodes.bin must also be in same directory as RMSort.bin
            fileMailSortArgs.Sort = consoleArgs.Carrier.GetSort(consoleArgs.MailMark, ResourcePath);

            // link status messages callbacks
            fileMailSortArgs.Sort.SettingUp += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.SetUpCompleted += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.Sorting += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.SortCompleted += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.Preparing += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.PrepareCompleted += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.ExtraProcessing += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.ExtraProcessingCompleted += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.ReportCreating += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.ReportCreated += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.AddCompleted += UseSort_StatusUpdate;

            // run the sort
            RunSort(fileMailSortArgs, consoleArgs.Carrier, consoleArgs.MailMark, consoleArgs.NationalSupplyChainId, consoleArgs.ZonalSupplyChainId);

            try
            {
                fileMailSortArgs.Sort.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Cannot dispose sort with exception {e}");
            }

            // pause and prompt user to exit
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        /// <summary>
        /// Call Sort object Setup, AddRecords, and Prepare to sort the input file
        /// Call Sort.ReadAll() to read the sorted records and write to args.OutputFile
        /// Create Planning Analysis Report, LineListing and BagLabels
        /// </summary>
        /// <param name="args">Arguments required to run a sort with input and output files</param>
        /// <param name="mailMark">Are we running a MailMark sort</param>
        /// <param name="carrier">Whistl, Citipost etc</param>
        /// <param name="nationalSupplyChainId">For MailMark sorts we need a national supply chain id or zonal supply chain id or both</param>
        /// <param name="zonalSupplyChainId">For MailMark sorts we need a national supply chain id or zonal supply chain id or both</param>
        private static void RunSort(MailSortArgs args, USEParams.Carriers carrier, bool mailMark, string nationalSupplyChainId, string zonalSupplyChainId)
        {
            try
            {
                args.Sort.Setup(args.Params);           // tell sort to prepare for record input
            }
            catch (ProductDetailsException e)
            {
                Console.WriteLine($"Error during sort.Setup with message {e.Message}");
                return;
            }
            catch (USELicenceException e)
            {
                Console.WriteLine($"Error during sort.Setup with message {e.Message}");
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error during sort.Setup {e}");
                return;
            }
           
            var added = AddRecords(args,carrier,mailMark);                        // add records from Input to the sort object

            // Wait until after AddRecords because pass records added to GetNextSupplyChainSequence which uses this count to prepare the control file for the next sort
            try
            {
                if (args.Params is USEDSAMailmarkParams)
                {
                    var mailMarkParams = args.Params as USEDSAMailmarkParams;
                    if (mailMarkParams == null) throw new Exception("dsa mailMarkParams should not be null");

                    // this increments both supply chain Id by the total input records. It leaves gaps in the sequencing but is safe and means we only lock the control file for a short while
                    if (mailMarkParams.DSASplitType == USEDSAParams.DSASplitTypes.Arbitrage || mailMarkParams.DSASplitType == USEDSAParams.DSASplitTypes.National)
                        mailMarkParams.StartingBarcodeValueNational = SupplyChainSequences.GetNextSupplyChainSequence(SupplyChainControlFile, nationalSupplyChainId, added, AttemptWait, Attempts, DisplaySupplyChainMessage, DisplaySupplyChainError);

                    if (mailMarkParams.DSASplitType == USEDSAParams.DSASplitTypes.Arbitrage || mailMarkParams.DSASplitType == USEDSAParams.DSASplitTypes.Zonal)
                        mailMarkParams.StartingBarcodeValueZonal = SupplyChainSequences.GetNextSupplyChainSequence(SupplyChainControlFile, zonalSupplyChainId, added, AttemptWait, Attempts, DisplaySupplyChainMessage, DisplaySupplyChainError);
                }
                else if (args.Params is USEMailmarkParams)
                {
                    var mailMarkParams = args.Params as USEMailmarkParams;
                    if (mailMarkParams == null) throw new Exception("mailMarkParams should not be null");

                    // royal mail is National only
                    mailMarkParams.StartingBarcodeValue = SupplyChainSequences.GetNextSupplyChainSequence(SupplyChainControlFile, nationalSupplyChainId, added, AttemptWait, Attempts, DisplaySupplyChainMessage, DisplaySupplyChainError);
                }
            }
            catch
            {
                Console.WriteLine("Unable to set StartingBarcodeValue");
                args.Sort.Prepare();                                // required for time being due to bug - Jira ticket USE-257
                return;
            }

            try
            {
                args.Sort.Prepare();                                // sets up counts, allocation sorts, and does any extra processing, i.e. the main work 
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception during sort prepare {exception}");
                return;
            }

            // keep track of some counts when retrieving records from the sort
            var counts = new MailSortCounts();

            // individually get each record from sort object and write Output
            using (var fileOut = new FileStream(args.OutputFile, FileMode.Create))
            {
                using (var sw = new StreamWriter(fileOut))
                {
                    var outputRecordNumber = 0;

                    // In this example item is either USEMailSortItemMailMark for Royal Mail MailMark or USEMailSortItemMailMarkDSA for other MailMark
                    // (Items could alternatively be USEMailSortItem or USEMailSortItemDSA for a sort that is not MailMark)
                    foreach (var item in args.Sort.ReadAll())
                    {
                        sw.WriteLine(item.ToString());
                        outputRecordNumber += 1;
                        if (outputRecordNumber % 100000 == 0)
                            Console.WriteLine($"Output {outputRecordNumber} records to {args.OutputFile}");

                        // This example is only for MailMark so this line not necessary but would be required if catering for other sorts 
                        if (!(item is IUSEableMailmark) && !(item is IUSEableMailmarkDSA)) continue;

                        // NB For MailMark you probably don't want to output to sorted file unless DRUIndicator != U
                        if (item.DRUIndicator != "U")
                            counts.MailMarkValid += 1;

                        if (args.Params.Carrier != USEParams.Carriers.Citipost && args.Params.Carrier != USEParams.Carriers.OnePost) continue;

                        if (item.SplitIndicator == "A")
                            counts.ZonalCount++;
                        else
                            counts.NationalCount++;
                    }
                    Console.WriteLine($"Output {outputRecordNumber} records to {args.OutputFile}");
                }
            }

            // will collate list of output files to output to the report XML
            var outputFiles = new List<ReportOutputFile>();

            // filepath, except extension, for planning analysis report
            var reportName = Path.Combine(OutputFolder, $"{Output}");                           

            if (args.Sort.SortStatus == SortStatuses.OK)
            {
                Console.WriteLine("create Line Listing required by all carriers");
                //args.Sort.GenerateCSVLineListing(OutputFolder, true);
                //outputFiles.Add(new ReportOutputFile(ReportOutputFile.LineListing, args.Sort.GenerateCSVLineListing(OutputFolder, true)));

                Console.WriteLine("create reports specific to each  of the four carriers");

                // in most cases true is the best option to go with this instructs the USE API to create filename
                // compatible with Cygnus and CitiPost/Whislt carrier convention
                // if false user needs to supply the full output filename rather than just the OutputFolder
                const bool apiGenerateFilename = true;

                switch (args.Sort)
                {
                    case USEMailSort<USEMailSortItem> royalMail:
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.BagLabels, royalMail.GenerateBagFile(OutputFolder, apiGenerateFilename)));
                        break;
                    case USESortMailmark<USEMailSortItemMailmark> royalMail:
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.BagLabels, royalMail.GenerateBagFile(OutputFolder, apiGenerateFilename)));
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.GemmaNational, royalMail.GenerateGemmaFile(OutputFolder, apiGenerateFilename)));
                        break;
                    case USESortCitipostMailmark<USEMailSortItemMailmarkDSA> citi:
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.BagLabels, citi.GenerateCitiPostBagFile(OutputFolder, apiGenerateFilename)));
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.BagLabels, citi.GenerateWhistlBagFile(OutputFolder, apiGenerateFilename)[0]));
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.CitipostTntUpload, citi.GenerateWhistlUploadFile(OutputFolder, apiGenerateFilename)[0]));
                        citi.GenerateGemmaFile(OutputFolder, apiGenerateFilename);
                        AddGemmaFiles(citi, outputFiles);
                        break;
                    case USESortWhistlMailmark<USEMailSortItemMailmarkDSA> whistl:
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.WhistlBagLabels, whistl.GenerateWhistlBagFile(OutputFolder, apiGenerateFilename)[0]));
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.WhistlTntUpload, whistl.GenerateWhistlUploadFile(OutputFolder, apiGenerateFilename)[0]));
                        whistl.GenerateGemmaFile(OutputFolder, apiGenerateFilename);
                        AddGemmaFiles(whistl, outputFiles);
                        break;
                    case USESortWhistl<USEMailSortItemDSA> whistl:
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.WhistlBagLabels, whistl.GenerateWhistlBagFile(OutputFolder, apiGenerateFilename)[0]));
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.WhistlTntUpload, whistl.GenerateWhistlUploadFile(OutputFolder, apiGenerateFilename)[0]));
                        break;
                    case USESortSecuredMailMailmark<USEMailSortItemMailmarkDSA> securedMail:
                        securedMail.GenerateGemmaFile(OutputFolder, apiGenerateFilename);
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.WhistlTntUpload, securedMail.GenerateSecuredMailBagFile(OutputFolder, apiGenerateFilename)));
                        AddGemmaFiles(securedMail, outputFiles);
                        break;
                    case USESortUKMailMailmark<USEMailSortItemMailmarkDSA> ukMail:
                        var gemmaFile = ukMail.GenerateGemmaFile(OutputFolder, apiGenerateFilename);
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.BagLabels, ukMail.GenerateUKMailBagFile(OutputFolder, true, apiGenerateFilename)));
                        outputFiles.Add(new ReportOutputFile($"Simple{ReportOutputFile.BagLabels}", ukMail.GenerateUKMailBagFile(OutputFolder, args.SimpleBag, apiGenerateFilename)));
                        AddGemmaFiles(ukMail, outputFiles);
                        break;
                    case USESortUKMail<USEMailSortItemDSA> ukMail:
                        outputFiles.Add(new ReportOutputFile(ReportOutputFile.BagLabels, ukMail.GenerateUKMailBagFile(OutputFolder, true, apiGenerateFilename)));
                        outputFiles.Add(new ReportOutputFile($"Simple{ReportOutputFile.BagLabels}", ukMail.GenerateUKMailBagFile(OutputFolder, args.SimpleBag, apiGenerateFilename)));
                        break;
                }
            }

            // generate the Planning Analysis Report regardless of whether the sort succeeds or fails

            outputFiles.Add(new ReportOutputFile(ReportOutputFile.PlanningAnalysis, $"{reportName}.pdf"));

            GenerateXmlData(args.Sort, $"{reportName}.xml", args.Sort.SortStatus == SortStatuses.OK, outputFiles);
            GeneratePlanningAnalysis(XslPath, reportName);

            if (args.Sort.SortStatus != SortStatuses.OK)
                Console.WriteLine($"Sort failed with SortStatus {args.Sort.SortStatus}");
        }

        /// <summary>
        /// Adds the relevant Gemma files to the outputFiles object which is dependent upon whether National, Zonal, or Arbitrage
        /// </summary>
        /// <param name="dsaMailMark">The DSA MailMark sort object</param>
        /// <param name="outputFiles">Object to add output files</param>
        private static void AddGemmaFiles(USESortMailmarkDSA<USEMailSortItemMailmarkDSA> dsaMailMark, List<ReportOutputFile> outputFiles)
        {
            if (!string.IsNullOrEmpty(dsaMailMark.MGemmaNameNational))
                outputFiles.Add(new ReportOutputFile(ReportOutputFile.GemmaNational, dsaMailMark.MGemmaNameNational));
            if (!string.IsNullOrEmpty(dsaMailMark.MGemmaNameZonal))
                outputFiles.Add(new ReportOutputFile(ReportOutputFile.GemmaZonal, dsaMailMark.MGemmaNameZonal));
        }

        /// <summary>
        /// Deep copy of DSAParameters node from fromXml to toXml.
        /// The DSA specific report data is supplied in separate report stream to the main report data so use this to copy into our XmlDocument
        /// </summary>
        /// <param name="fromXmlStream">XmlDocument containing the DSAParameters node</param>
        /// <param name="toXml">XmlDocument we want to copy DSAParameters into</param>
        private static void CopyAcrossDsaParameters(Stream fromXmlStream, XmlDocument toXml)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(fromXmlStream);

            var dsaNode = xmlDoc.DocumentElement?.SelectSingleNode("DSAParameters");
            if (dsaNode != null)
            {
                var insertNode = toXml.ImportNode(dsaNode, true);
                toXml.DocumentElement?.AppendChild(insertNode);
            }
        }

        /// <summary>
        /// Outputs the XML report data Stream from USE to disk file whilst adding extra tags
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="outputXml"></param>
        /// <param name="ok"></param>
        private static void GenerateXmlData(IUSESort sort, string outputXml, bool ok, List<ReportOutputFile> outputFiles)
        {
            var xmlDocument = new XmlDocument();

            // load the XmlReport document into memory so that we can manipulate as necessary
            xmlDocument.Load(sort.Report());

            // add application specific tags to the XmlOutput
            ApplicationInformation.AppendTextElement(xmlDocument, xmlDocument.DocumentElement, "SortOK", ok.ToString());
            ApplicationInformation.Add(xmlDocument);

            if (!(sort is IUSESortMailmark))
            {
                // Royal Mail rates are public knowledge and so we add these rates into the sort.Report XML stream.
                // For DSAs we do not hold the rates in USE as DSA rates are specific to each carrier and DSA client.
                // Therefore for DSAs we add the tags with value 0 and they can be set here to make the planning analysis report more useful
                AddRate(xmlDocument, "//Report/PlanningAnalysis/Savings/StandardTariffRate", 25.9);
                AddRate(xmlDocument, "//Report/PlanningAnalysis/Costs/ResidueRate", 21.1);

                AddRate(xmlDocument, "//Report/PlanningAnalysis/StandardTariffCostAnalysis/EuropeRate", 30.1);
                AddRate(xmlDocument, "//Report/PlanningAnalysis/StandardTariffCostAnalysis/Zone1Rate", 31.1);
                AddRate(xmlDocument, "//Report/PlanningAnalysis/StandardTariffCostAnalysis/Zone2Rate", 32.1);
                AddRate(xmlDocument, "//Report/PlanningAnalysis/StandardTariffCostAnalysis/UKUnmatchedRate", 33.1);
                AddRate(xmlDocument, "//Report/PlanningAnalysis/StandardTariffCostAnalysis/UKWithdrawnRate", 34.1);
            }

            switch (sort)
            {
                // The DSAParameters are contained within a separate XmlStream so we need to read and add to our Output.xml
                case IUSESortCitipost citiPost:
                    CopyAcrossDsaParameters(citiPost.CitipostXML(), xmlDocument);
                    break;
                case IUSESortCitipostMailmark citiPostMailmark:
                    CopyAcrossDsaParameters(citiPostMailmark.CitipostXML(), xmlDocument);
                    break;
                case IUSESortWhistl whistl:
                    CopyAcrossDsaParameters(whistl.WhistlXML(), xmlDocument);
                    break;
                case IUSESortWhistlMailmark whistlMailmark:
                    CopyAcrossDsaParameters(whistlMailmark.WhistlXML(), xmlDocument);
                    break;
                case IUSESortSecuredMail securedMail:
                    CopyAcrossDsaParameters(securedMail.SecuredMailXML(), xmlDocument);
                    break;
                case IUSESortSecuredMailMailmark securedMailMailmark:
                    CopyAcrossDsaParameters(securedMailMailmark.SecuredMailXML(), xmlDocument);
                    break;
                case IUSESortUKMail ukMail:
                    CopyAcrossDsaParameters(ukMail.UKMailXml(), xmlDocument);
                    break;
                case IUSESortUKMailMailmark ukMailMailmark:
                    CopyAcrossDsaParameters(ukMailMailmark.UKMailXml(), xmlDocument);
                    break;
            }

            if (outputFiles.Count > 0)
            {
                var node = xmlDocument.CreateNode(XmlNodeType.Element, "OutputFiles", null);
                if (xmlDocument.DocumentElement == null) throw new Exception("DocumentElement is null in GenerateXmlData()");
                xmlDocument.DocumentElement.AppendChild(node);
                foreach (var file in outputFiles)
                {
                    var outputNode = xmlDocument.CreateNode(XmlNodeType.Element, file.FileDescription, null);
                    outputNode.InnerText = Path.GetFullPath(file.OutputLocation);
                    node.AppendChild(outputNode);
                }
            }

            // save USE report tags and our application specified tags to the XmlOutput
            xmlDocument.Save(outputXml);
        }

        /// <summary>
        /// Changes the value of the InnerText at xPath to the rate specified
        /// </summary>
        /// <param name="xmlDocument"></param>
        /// <param name="xPath"></param>
        /// <param name="rate"></param>
        private static void AddRate(XmlDocument xmlDocument, string xPath, double rate)
        {
            var node = xmlDocument.SelectSingleNode(xPath);
            if (node != null)
                node.InnerText = $"{rate}";
        }

        /// <summary>
        /// Use XML Data from USE Sort object to generate Planning Analysis Report using XSLT, XSL-FO and FONet
        /// </summary>
        /// <param name="xslPath"></param>
        /// <param name="outputName"></param>
        private static void GeneratePlanningAnalysis(string xslPath, string outputName)
        {
            // Get the full path for these as we change the current directory below before creating the report
            var xslFo = Path.GetFullPath(outputName + "-XSL-FO.xml");
            var pdfOutput = Path.GetFullPath(outputName + ".pdf");

            // Load the XSLT to FO transformation
            var xslt = new XslCompiledTransform();
            
            xslt.Load(xslPath);

            // Execute the transform and output the results to the XSL FO file
            xslt.Transform(outputName + ".xml", xslFo);

            try
            {
                // Run from inside the XSL directory so that Fonet can find USE.png
                // we save CurrentDirectory and restore after creating the PDF
                var saveDirectory = Environment.CurrentDirectory;
                try
                {
                    Environment.CurrentDirectory = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule?.FileName) + @"\..\..\XSL\";
                    var driver = FonetDriver.Make();
                    driver.Render(xslFo, pdfOutput);
                    Process.Start(pdfOutput);
                }
                finally
                {
                    Environment.CurrentDirectory = saveDirectory;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.Contains("The requested operation cannot be performed on a file with a user-mapped section open.")
                    ? "Unable to write to output file"
                    : $"Exception {e}");

                Console.WriteLine("enter to proceed");
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Much simpler version to use for UnSorted only need to worry about USEMailSortItem
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static int AddRecords(UnSortedArgs args, bool mailMark)
        {
            var record = 0;
            using (var reader = new StreamReader(args.InputFile))
            {
                var line = reader.ReadLine();                               // drop header record
                var hasWeightField = line.IndexOf("WEIGHT") > -1;
                var hasOverseasField = line.IndexOf("OVERSEAS") > -1;

                var inputRecordNumber = 0;

                while ((line = reader.ReadLine()) != null)
                {
                    inputRecordNumber += 1;

                    if (mailMark)
                    {
                        var item = ConvertToMailSortItem<USEMailSortItemMailmarkDSA>(record, line, hasWeightField, hasOverseasField);

                        if (args.Params is USEDSAMailmarkParams pDSA)
                        {
                            if (!string.IsNullOrEmpty(pDSA.CustomerRef))
                                item.CustomerRef = inputRecordNumber.ToString();
                        }
                        else
                            throw new Exception("Unexpected Params object in AddRecords (unsorted)");

                        args.Sort.Add(item);
                    }
                    else
                        args.Sort.Add(ConvertToMailSortItem<USEMailSortItem>(record, line, hasWeightField, hasOverseasField));

                    record += 1;
                }
            }

            return record;
        }

        /// <summary>
        /// Input file must be pipe delimited with the following fields and header record A1|A2|A3|A4|A5|A6|A7|Town|Zip|DPS
        /// with optional |ItemWeight
        /// </summary>
        /// <param name="args"></param>
        /// <param name="carrier"></param>
        /// <param name="mailMark"></param>
        private static int AddRecords(MailSortArgs args, USEParams.Carriers carrier, bool mailMark)
        {
            var record = 0;
            using (var reader = new StreamReader(args.InputFile))
            {
                var royalMail = carrier== USEParams.Carriers.RoyalMail;      // test once 

                var line = reader.ReadLine();                                      // drop header record
                var hasWeightField = line.IndexOf("WEIGHT") > -1;
                var hasOverseasField = line.IndexOf("OVERSEAS") > -1;

                var inputRecordNumber = 0;

                while ((line = reader.ReadLine()) != null)
                {
                    inputRecordNumber += 1;
                    if (royalMail)
                    {
                        // add a RoyalMail MailMark item to the sort
                        if (!mailMark)
                            args.Sort.Add(ConvertToMailSortItem<USEMailSortItem>(record, line, hasWeightField, hasOverseasField));
                        else
                        {
                            var item = ConvertToMailSortItem<USEMailSortItemMailmark>(record, line, hasWeightField, hasOverseasField);
                            // for demo purposes we apply a sequential number to the customer reference if /CustomerRef=fieldname has been supplied as command line parameter
                            if (!string.IsNullOrEmpty((args.Params as USEMailmarkParams).CustomerRef))
                                item.CustomerRef = inputRecordNumber.ToString();                            
                            args.Sort.Add(item);
                        }
                    }
                    else
                    {
                        // add a DSA MailMark item to the sort
                        if (!mailMark)
                            args.Sort.Add(ConvertToMailSortItem<USEMailSortItemDSA>(record, line, hasWeightField, hasOverseasField));
                        else
                        {
                            var item = ConvertToMailSortItem<USEMailSortItemMailmarkDSA>(record, line, hasWeightField, hasOverseasField);
                            // for demo purposes we apply a sequential number to the customer reference if /CustomerRef=fieldname has been supplied as command line parameter
                            if (!string.IsNullOrEmpty((args.Params as USEDSAMailmarkParams).CustomerRef))
                                item.CustomerRef = inputRecordNumber.ToString();
                            args.Sort.Add(item);
                        }
                    }

                    record += 1;
                }
            }

            return record;
        }

        private static T ConvertToMailSortItemBigFile<T>(int recordNumber, string pipeDelimitedString, bool hasWeight, bool hasOverseas, string[] fields) where T : IUSEable, new()
        {
            var addressLines = new string[9];
            addressLines[0] = fields[1];
            addressLines[1] = fields[2];
            addressLines[2] = fields[3];
            addressLines[3] = fields[4];
            addressLines[4] = fields[5];
            addressLines[5] = fields[6];
            addressLines[6] = fields[7];
            addressLines[7] = fields[8];
            addressLines[8] = fields[40];

            var pcBits = fields[8].Split(' ');

            var item = new T
            {
                URN = recordNumber.ToString(),
                AddLines = addressLines,
                Town = "",                                  // Town not in consistent location
                UserDefined = pipeDelimitedString,
            };

            if (fields.Length > 66)
                item.DPS = fields[66];

            if (pcBits.Length > 0)
                item.OutCode = pcBits[0];
            if (pcBits.Length > 1)
                item.Incode = pcBits[1];

            return item;
        }

        /// <summary>
        /// Converts the pipe delimited string with required fields to Item object that can be fed into sort object
        /// </summary>
        /// <typeparam name="T">MailSortItem will be USEMailSortItem in this example</typeparam>
        /// <param name="recordNumber">Used to create a sequential "URN"</param>
        /// <param name="pipeDelimitedString">The input pipe delimited record</param>
        /// <param name="hasWeight">Do we have a weight field on the input file</param>
        /// <returns></returns>
        private static T ConvertToMailSortItem<T>(int recordNumber, string pipeDelimitedString, bool hasWeight, bool hasOverseas) where T : IUSEable, new()
         {
            var fields = pipeDelimitedString.Split('|');

            if (fields.Length < 11)
                throw new Exception("Input record has less than 11 fields");

            if (fields.Length > 60)
                return ConvertToMailSortItemBigFile<T>(recordNumber, pipeDelimitedString, hasWeight, hasOverseas, fields);

            var addressLines = new string[7];
            addressLines[0] = fields[0];
            addressLines[1] = fields[1];
            addressLines[2] = fields[2];
            addressLines[3] = fields[3];
            addressLines[4] = fields[4];
            addressLines[5] = fields[5];
            addressLines[6] = fields[6];

            var item = new T
            {
                URN = recordNumber.ToString(),
                AddLines = addressLines,
                Town = fields[7],
                OutCode = fields[8],
                Incode = fields[9],
                UserDefined = pipeDelimitedString,
                DPS = fields[10],
            };

            if (hasWeight)
                item.Weight = Convert.ToInt32(fields[11]);

            // NB check there is actually a 13th field in the data as don't insist this field is present in the data
            if (hasOverseas && fields.Length > 12) 
                item.Overseas = fields[12].ToUpper() == "T";

            return item;
        }

        /// <summary>
        /// Used as callback to get messages from GetNextSupplyChainSequence and GetNextSegmentCode
        /// </summary>
        /// <param name="message">Message to be displayed</param>
        private static void DisplaySupplyChainMessage(string message)
        {
            Console.WriteLine(message);
        }

        /// <summary>
        /// Used as callback to get messages from GetNextSupplyChainSequence and GetNextSegmentCode
        /// </summary>
        /// <param name="message">Message to be displayed</param>
        /// <param name="error">Exception to be displayed</param>
        private static void DisplaySupplyChainError(string message, Exception error)
        {
            Console.WriteLine(error != null ? $"{message} with exception {error}" : message);
        }

        /// <summary>
        /// UnSorted are very different to MailMark etc to handle in separate method
        /// </summary>
        /// <param name="consoleArgs"></param>
        private static void UnSorted(ConsoleArgs consoleArgs)
        {
            if (consoleArgs.Carrier != USEParams.Carriers.Whistl && consoleArgs.Carrier != USEParams.Carriers.UKMail && consoleArgs.Carrier != USEParams.Carriers.RoyalMail)
            {
                Console.WriteLine($"UnSorted not permitted with {consoleArgs.Carrier}");
                return;
            }

            // set a default MaxContainerContentWeight if the user has not specifically set it
            if (consoleArgs.MaxContainerContentWeight == 0)
                consoleArgs.SetDefaultMaxContainerContentWeight();

            Console.WriteLine($"user parameters - Test: {consoleArgs.MailingDesc }, {consoleArgs.Carrier}, unsorted: { consoleArgs.Unsorted}");
            Console.WriteLine($".............. -   {consoleArgs.InputFile}, {consoleArgs.Container} weight: {consoleArgs.ItemWeight}");
            Console.WriteLine($".............. -  container: {consoleArgs.Container}, maxwgt: {consoleArgs.MaxContainerContentWeight}");
            Console.WriteLine($".............. -  supply Chain Id: {consoleArgs.NationalSupplyChainId}, maxwgt: {consoleArgs.MaxContainerContentWeight}, Mailmark Customer Reference Field: {consoleArgs.CustomerReferenceField}");

            // create object to hold all parameters required to run a sort
            var fileUnSortedArgs = new UnSortedArgs
            {
                InputFile = string.IsNullOrEmpty(consoleArgs.InputFile) ? @"..\..\TestData\Input.psv" : consoleArgs.InputFile,
                OutputFile = Path.Combine(OutputFolder, $"{Output}.psv"),
            };

            if (consoleArgs.Carrier == USEParams.Carriers.Whistl && !consoleArgs.MailMark)
            {
                fileUnSortedArgs.Sort = new USEUnsort<USEMailSortItem>();
                fileUnSortedArgs.Params = new USEDSAParams();
            }
            else if (consoleArgs.Carrier == USEParams.Carriers.UKMail && consoleArgs.MailMark)
            {
                //fileUnSortedArgs.Sort = new USEUnsortedMailmark<USEMailSortItemMailmark>();
                fileUnSortedArgs.Sort = new USEUnsortedMailmark<USEMailSortItemMailmarkDSA>();
                fileUnSortedArgs.Params = new USEDSAMailmarkParams();
            }
            else if (consoleArgs.Carrier == USEParams.Carriers.RoyalMail && consoleArgs.MailMark)
            {
                fileUnSortedArgs.Sort = new USEUnsortedMailmark<USEMailSortItemMailmarkDSA>();
                fileUnSortedArgs.Params = new USEDSAMailmarkParams();
            }
            else if (consoleArgs.Carrier == USEParams.Carriers.RoyalMail && !consoleArgs.MailMark)
            {
                fileUnSortedArgs.Sort = new USEUnsort<USEMailSortItem>();
                fileUnSortedArgs.Params = new USEParams();
            }
            else 
            {
                Console.WriteLine($"UnSorted not permitted with {consoleArgs.Carrier} when MailMark={consoleArgs.MailMark}");
                return;
            }

            if (fileUnSortedArgs.Params is USEDSAParams || fileUnSortedArgs.Params is USEDSAMailmarkParams)
                Console.WriteLine($".............. -  DSA Machinable: {consoleArgs.DSAMachinable}");

            consoleArgs.Carrier.SetParams(fileUnSortedArgs.Params, consoleArgs);

            // Extra work for Whistl - need to set the Whistl Segment Code (preparation for other carriers unsorted)
            if (consoleArgs.Carrier == USEParams.Carriers.Whistl)
            {
                (fileUnSortedArgs.Params as USEDSAParams).WhistlSegmentCode = WhistlSegmentCode.GetNextSegmentCode(WhistlSegmentControlFile, AttemptWait, Attempts, DisplaySupplyChainMessage, DisplaySupplyChainError);
                if (string.IsNullOrEmpty((fileUnSortedArgs.Params as USEDSAParams).WhistlSegmentCode))
                {
                    Console.WriteLine("error getting next Whistl Segment Code - any key to exit");
                    Console.ReadKey();
                    return;
                }
            }

            // link status messages callbacks
            fileUnSortedArgs.Sort.SettingUp += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.SetUpCompleted += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.Sorting += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.SortCompleted += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.Preparing += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.PrepareCompleted += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.ExtraProcessing += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.ExtraProcessingCompleted += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.ReportCreating += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.ReportCreated += UseSort_StatusUpdate;
            fileUnSortedArgs.Sort.AddCompleted += UseSort_StatusUpdate;

            try
            {
                if (fileUnSortedArgs.Sort is USEUnsortedMailmark<USEMailSortItemMailmarkDSA> mailMark)
                    mailMark.Setup(fileUnSortedArgs.Params, ResourcePath);
                else
                    fileUnSortedArgs.Sort.Setup(fileUnSortedArgs.Params);
            }
            catch (ProductDetailsException e)
            {
                Console.WriteLine($"Error during sort.Setup with message {e.Message}");
                return;
            }
            catch (USELicenceException e)
            {
                Console.WriteLine($"Error during sort.Setup with message {e.Message}");
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error during sort.Setup {e}");
                return;
            }

            var added = AddRecords(fileUnSortedArgs, consoleArgs.MailMark);                      // add records from Input to the sort object

            // Wait until after AddRecords because pass records added to GetNextSupplyChainSequence which uses this count to prepare the control file for the next sort
            try
            {
                if (fileUnSortedArgs.Params is USEDSAMailmarkParams)
                {
                    var mailMarkParams = fileUnSortedArgs.Params as USEDSAMailmarkParams;
                    if (mailMarkParams == null) throw new Exception("dsa mailMarkParams should not be null");
                    
                    if (mailMarkParams.DSASplitType != USEDSAParams.DSASplitTypes.National)
                    {
                        Console.WriteLine("SplitType must be National for UnSorted mailMark");
                        fileUnSortedArgs.Sort.Prepare();                                        // required for time being due to bug - Jira ticket USE-257
                        return;
                    }

                    // this increments both supply chain Id by the total input records. It leaves gaps in the sequencing but is safe and means we only lock the control file for a short while
                    mailMarkParams.StartingBarcodeValueNational = SupplyChainSequences.GetNextSupplyChainSequence(SupplyChainControlFile, consoleArgs.NationalSupplyChainId, added, AttemptWait, Attempts, DisplaySupplyChainMessage, DisplaySupplyChainError);
                }
            }
            catch
            {
                Console.WriteLine("Unable to set StartingBarcodeValue");
                fileUnSortedArgs.Sort.Prepare();                                // required for time being due to bug - Jira ticket USE-257
                return;
            }

            try
            {
                fileUnSortedArgs.Sort.Prepare();                              // sets up counts, allocation sorts, and does any extra processing, i.e. the main work 
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception during sort prepare {exception}");
                return;
            }

            // individually get each record from sort object and write Output
            using (var fileOut = new FileStream(fileUnSortedArgs.OutputFile, FileMode.Create))
            {
                using (var sw = new StreamWriter(fileOut))
                {
                    var outputRecordNumber = 0;

                    foreach (var item in fileUnSortedArgs.Sort.ReadAll())
                    {
                        sw.WriteLine(item.ToString());
                        outputRecordNumber += 1;
                        if (outputRecordNumber % 100000 == 0)
                             Console.WriteLine($"Output {outputRecordNumber} records to {fileUnSortedArgs.OutputFile}");
                    }
                    Console.WriteLine($"Output {outputRecordNumber} records to {fileUnSortedArgs.OutputFile}");
                }
            }

            // generate the Planning Analysis Report regardless of whether the sort succeeds or fails
            GenerateUnsortedXmlData(fileUnSortedArgs.Sort, Path.Combine(OutputFolder, $"{Output}.xml"), fileUnSortedArgs.Sort.SortStatus == SortStatuses.OK);
            GeneratePlanningAnalysis(XslPath, Path.Combine(OutputFolder, $"{Output}"));
            GenerateBagBreakdownListing(BagBreakdownXslPath, Path.Combine(OutputFolder, $"{Output}"));

            // in most cases true is the best option to go with this instructs the USE API to create filename
            // compatible with Cygnus and CitiPost/Whislt carrier convention
            // if false user needs to supply the full output filename rather than just the OutputFolder
            const bool apiGenerateFilename = true;

            if (fileUnSortedArgs.Params.Carrier == USEParams.Carriers.UKMail)
                (fileUnSortedArgs.Sort as USEUnsortedMailmark<USEMailSortItemMailmarkDSA>).GenerateUKMailBagFile(OutputFolder, consoleArgs.SimpleBag, apiGenerateFilename, ResourcePath);

            if (consoleArgs.MailMark)
                (fileUnSortedArgs.Sort as USEUnsortedMailmark<USEMailSortItemMailmarkDSA>).GenerateGemmaFile(OutputFolder, true, true);

            if (fileUnSortedArgs.Sort.SortStatus != SortStatuses.OK)
            {
                Console.WriteLine($"Sort failed with SortStatus {fileUnSortedArgs.Sort.SortStatus}");
                return;
            }

            try
            {
                fileUnSortedArgs.Sort.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Cannot dispose sort with exception {e}");
            }
        }

        /// <summary>
        /// Deep copy of DSAParameters node from fromXml to toXml.
        /// The DSA specific report data is supplied in separate report stream to the main report data so use this to copy into our XmlDocument
        /// </summary>
        /// <param name="fromXmlStream">XmlDocument containing the DSAParameters node</param>
        /// <param name="toXml">XmlDocument we want to copy DSAParameters into</param>
        private static void CopyAcrossBagBreak(Stream fromXmlStream, XmlDocument toXml)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(fromXmlStream);

            var dsaNode = xmlDoc.DocumentElement;
            if (dsaNode != null)
            {
                var insertNode = toXml.ImportNode(dsaNode, true);
                toXml.DocumentElement?.AppendChild(insertNode);
            }
        }

        /// <summary>
        /// Outputs the XML report data Stream from USE to disk file whilst adding extra tags
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="outputXml"></param>
        /// <param name="ok"></param>
        private static void GenerateUnsortedXmlData(IUSEUnsorted sort, string outputXml, bool ok)
        {
            var xmlDocument = new XmlDocument();

            // load the XmlReport document into memory so that we can manipulate as necessary
            xmlDocument.Load(sort.PlanningAnalysisReport());

            // add application specific tags to the XmlOutput
            ApplicationInformation.AppendTextElement(xmlDocument, xmlDocument.DocumentElement, "SortOK", ok.ToString());
            ApplicationInformation.Add(xmlDocument);

            CopyAcrossBagBreak(sort.BagBreakdownReport(), xmlDocument);

            // save USE report tags and our application specified tags to the XmlOutput
            xmlDocument.Save(outputXml);
        }

        private static void GenerateBagBreakdownListing(string xslPath, string outputName)
        {
            var xslt = new XslCompiledTransform();
            xslt.Load(xslPath);
            xslt.Transform(outputName + ".xml", outputName + "-BagListing.txt");
        }
    }
}
