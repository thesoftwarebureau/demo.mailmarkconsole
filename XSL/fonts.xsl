<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">  
    <!-- Fonts -->
    <!-- detail font 1 -->
    <xsl:variable name="d1-family" select="'Courier'"/>
    <xsl:variable name="d1-size" select="'10pt'"/>
    <!-- detail font 3 -->
    <xsl:variable name="d3-family" select="'Courier'"/>
    <xsl:variable name="d3-size" select="'8pt'"/>
    <!-- detail font 4 -->
    <xsl:variable name="d4-family" select="'Courier'"/>
    <xsl:variable name="d4-size" select="'7pt'"/>
    <!-- h1 not used as yet -->
    <xsl:variable name="h1-family" select="'Times'"/>
    <xsl:variable name="h1-size" select="'18pt'"/>
    <!-- h2 not used as yet -->
    <xsl:variable name="h2-family" select="'Times'"/>
    <xsl:variable name="h2-size" select="'16pt'"/>
    <!-- h3 not used as yet -->
    <xsl:variable name="h3-family" select="'Times'"/>
    <xsl:variable name="h3-size" select="'14pt'"/>
    <!-- h4 not used as yet -->
    <xsl:variable name="h4-family" select="'Times'"/>
    <xsl:variable name="h4-size" select="'12pt'"/>
    <!-- h5 -->
    <xsl:variable name="h5-family" select="'Times'"/>
    <xsl:variable name="h5-size" select="'10pt'"/>
    <!-- h6 -->
    <xsl:variable name="h6-family" select="'Times'"/>
    <xsl:variable name="h6-size" select="'9pt'"/>
    <!-- h7 -->
    <xsl:variable name="h7-family" select="'Times'"/>
    <xsl:variable name="h7-size" select="'8pt'"/>
    <xsl:variable name="h7-weight" select="'bold'"/>
    <!-- h8 -->
    <xsl:variable name="h8-family" select="'Times'"/>
    <xsl:variable name="h8-size" select="'7pt'"/>
    <xsl:variable name="h8-weight" select="'bold'"/>
</xsl:stylesheet>
