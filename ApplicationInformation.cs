﻿using System;
using System.IO;
using System.Reflection;
using System.Xml;

namespace DSAMailMarkConsole
{
    /// <summary>
    /// ApplicationInformation is output to BannerPage tag in Xml document merged with report details output by USE Sort
    /// </summary>
    public class ApplicationInformation
    {
        /// <summary>
        /// Append a new text node to parentNode
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="parentNode"></param>
        /// <param name="title"></param>
        /// <param name="value"></param>
        public static void AppendTextElement(XmlDocument doc, XmlNode parentNode, string title, string value)
        {
            XmlElement element = doc.CreateElement(title);
            element.InnerText = value;
            parentNode.AppendChild(element);
        }

        /// <summary>
        /// Add application specific details to report stream so that it can be ingested by XSL-FO processor
        /// </summary>
        /// <param name="toXml"></param>
        public static void Add(XmlDocument toXml)
        {
            CopyAcrossXmlDocument(ProjectDataStream(), toXml);
        }

        /// <summary>
        /// Copy from one XmlStream (fromXmlStream) to an XmlDocument (toXml)
        /// </summary>
        /// <param name="fromXmlStream"></param>
        /// <param name="toXml"></param>
        private static void CopyAcrossXmlDocument(Stream fromXmlStream, XmlDocument toXml)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(fromXmlStream);

            var dsaNode = xmlDoc.DocumentElement;
            if (dsaNode != null)
            {
                var insertNode = toXml.ImportNode(dsaNode, true);
                toXml.DocumentElement?.AppendChild(insertNode);
            }
        }

        /// <summary>
        /// Converts Xml Stream of Project type information about the sort
        /// </summary>
        /// <returns></returns>
        private static Stream ProjectDataStream()
        {
            var doc = new XmlDocument();
            var bannerNode = doc.CreateElement("ApplicationData");
            doc.AppendChild(bannerNode);

            var projectNode = doc.CreateElement("Project");
            var processNode = doc.CreateElement("Process");
            var versionNode = doc.CreateElement("Version");
            var userNode = doc.CreateElement("User");

            bannerNode.AppendChild(projectNode);
            bannerNode.AppendChild(processNode);
            bannerNode.AppendChild(userNode);
            processNode.AppendChild(versionNode);

            AppendTextElement(doc, projectNode, "Client", "ABC Ltd");
            AppendTextElement(doc, projectNode, "ProjectName", "Clean Customer Data");
            AppendTextElement(doc, projectNode, "ProjectId", "1");
            AppendTextElement(doc, projectNode, "ProjectReference", "MailCellReference");

            AppendTextElement(doc, processNode, "Date", DateTime.Now.ToString("dd/MM/yyyy"));
            AppendTextElement(doc, processNode, "Time", DateTime.Now.ToString("H:mm:ss"));
            AppendTextElement(doc, versionNode, "SwiftSort", ApplicationVersion());

            AppendTextElement(doc, userNode, "UserName", "Paul Callow");
            AppendTextElement(doc, userNode, "Company", "The Software Bureau Ltd");
            AppendTextElement(doc, userNode, "Telephone", "00000 111111");
            AppendTextElement(doc, userNode, "Fax", "00000 222222");
            AppendTextElement(doc, userNode, "WWW", "www.thesoftwarebureau.com");

            var addressNode = doc.CreateElement("Address");
            userNode.AppendChild(addressNode);
            foreach (var line in new string[] { "Airport House, Purley Way, Croydon, CR0 0XZ"})
                AppendTextElement(doc, addressNode, "Line", line);

            var xmlStream = new MemoryStream();
            doc.Save(xmlStream);
            xmlStream.Flush();
            xmlStream.Position = 0;
            return xmlStream;
        }

        private static string ApplicationVersion()
        {
            try
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
