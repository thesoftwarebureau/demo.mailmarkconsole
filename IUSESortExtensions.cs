﻿using System;
using System.IO;
using SoftwareBureau.UniversalSortEngine;
using SoftwareBureau.UniversalSortEngine.MailSort;

namespace DSAMailMarkConsole
{
    /// <summary>
    /// The parameters this application received from command prompt input
    /// </summary>
    public class ConsoleArgs
    {
        /// <summary>
        /// If Whistl and Unsorted not MailMark otherwise check the SortType specified
        /// </summary>
        public bool MailMark => SortType == USEParams.SortTypes.Mailmark;

        public USEParams.Carriers Carrier { get; set; }

        public USEDSAParams.DSASplitTypes SplitType { get; set; }

        public string NationalSupplyChainId { get; set; }

        public string ZonalSupplyChainId { get; set; }

        public bool Unsorted { get; set; } = true;                                                  // default is false

        public USEParams.ServiceLevels ServiceLevel { get; set; } = USEParams.ServiceLevels.Second; // default to what the Params object would

        public USEParams.Pieces Piece { get; set; } = USEParams.Pieces.Letter;                      // default to what the Params object would

        public USEParams.Containers Container { get; set; } = USEParams.Containers.Bags;            // default to what the Params object would

        public USEParams.MailTypes MailType { get; set; } = USEParams.MailTypes.Business;

        public bool Sort48 { get; set; } = false;

        public int ItemWeight { get; set; } = 50;

        public int MinimumSelectionSize { get; set; } = 50;

        public int MaxContainerContentWeight { get; set; } = 0;

        public bool AllowSelectionTrayWithdrawal { get; set; } = false;       

        public bool SwitchOffVolumeChecking { get; set; } = false;

		public bool AllowAutoDpsLevelAdjustment { get; set; } = true;

        public USEParams.Sustainabilities Sustainabilities { get; set; } = USEParams.Sustainabilities.Standard;

        public string MailingDesc { get; set; } = "No Test";
        
        public string InputFile { get; set; } = @"..\..\InputData\input.psv";

        public USEParams.DirectResidueOrders DirectResidueOrder { get; set; } = USEParams.DirectResidueOrders.AlternateDirectResidue;

        public bool SimpleBag { get; set; } = false;

        public USEParams.SortTypes SortType { get; set; } = USEParams.SortTypes.Mailmark;

        public bool DSAMachinable { get; internal set; } = true;

        public bool Partial { get; set; } = false;

        public string CustomerReferenceField { get; set; }          // name of field containing the customer reference to use to populate Gemma Spare9 field for Mailmark sorts

        public string MailmarkBatchReference { get; set; }         // the unique batch reference string for mailmark

        public void SetDefaultMaxContainerContentWeight()
        {
            switch (Container)
            {
                case USEParams.Containers.Trays:
                    MaxContainerContentWeight = 9000;
                    break;
                case USEParams.Containers.Bags:
                    MaxContainerContentWeight = 10800;
                    break;
                case USEParams.Containers.Bundles:
                    MaxContainerContentWeight = 6400;
                    break;
                default:
                    MaxContainerContentWeight = 245000;
                    break;
            }
        }
    }

    public static class IuseSortExtensions
    {
        /// <summary>
        /// Returns a IUSESort for the carrier and of the sort type specified
        /// </summary>
        /// <remarks>
        /// Although this example only demonstrates MailMark sorts this method would work for any USE engine sort
        /// </remarks>
        /// <param name="carrier">RoyalMail, CitiPost etc must be specified as we return a different IUSESort object for each Carrier</param>
        /// <param name="mailMark">A different IUSESort object is also required for MailMark sorts </param>
        /// <param name="mailSortBinFile">Full file/path name of binary file containing the MailSort tables e.g. c:\db\RMSort.bin</param>
        /// <returns>Carrier/Mailmark specific IUSESort object</returns>
        public static IUSESort GetSort(this USEParams.Carriers carrier, bool mailMark, string mailSortBinFile)
        {
            switch (carrier)
            {
                case USEParams.Carriers.RoyalMail:
                    return mailMark
                        ? (IUSESort) new USESortMailmark<USEMailSortItemMailmark>(mailSortBinFile)
                        : new USEMailSort<USEMailSortItem>(mailSortBinFile);
                case USEParams.Carriers.Citipost:
                    return mailMark
                        ? (IUSESort) new USESortCitipostMailmark<USEMailSortItemMailmarkDSA>(mailSortBinFile)
                        : new USESortCitipost<USEMailSortItemDSA>(mailSortBinFile);
                case USEParams.Carriers.SecuredMail:
                    return mailMark
                        ? (IUSESort) new USESortSecuredMailMailmark<USEMailSortItemMailmarkDSA>(mailSortBinFile)
                        : new USESortSecuredMail<USEMailSortItemDSA>(mailSortBinFile);
                case USEParams.Carriers.UKMail:
                    return mailMark
                        ? (IUSESort) new USESortUKMailMailmark<USEMailSortItemMailmarkDSA>(mailSortBinFile)
                        : new USESortUKMail<USEMailSortItemDSA>(mailSortBinFile);
                case USEParams.Carriers.Whistl:
                    return mailMark
                        ? (IUSESort) new USESortWhistlMailmark<USEMailSortItemMailmarkDSA>(mailSortBinFile)
                        : new USESortWhistl<USEMailSortItemDSA>(mailSortBinFile);
                case USEParams.Carriers.OnePost:
                    return mailMark
                        ? (IUSESort) new USESortOnePostMailmark<USEMailSortItemMailmarkDSA>(mailSortBinFile)
                        : new USESortOnePost<USEMailSortItemDSA>(mailSortBinFile);
                default:
                    throw new Exception($"Unexpected Carrier {carrier}");
            }
        }

        /// <summary>
        /// The parameter object depends upon Carrier and whether the sort is a MailMark sort
        /// </summary>
        /// <remarks>
        /// This method will work for all sorts rather than just the sorts covered by this demo
        /// </remarks>
        /// <param name="carrier"></param>
        /// <param name="mailMark"></param>
        /// <returns>Returns correct parameter object dependent upon carrier and whether the sort is MailMark</returns>
        public static USEParams GetParams(this USEParams.Carriers carrier, bool mailMark)
        {
            if (!mailMark)
                return carrier == USEParams.Carriers.RoyalMail ? new USEParams() : new USEDSAParams();

            if (carrier == USEParams.Carriers.RoyalMail)
                return new USEMailmarkParams();

            return new USEDSAMailmarkParams();
        }

        /// <summary>
        /// Sets up parameter object by applying fixed values
        /// </summary>
        /// <remarks>
        /// In live application these parameters would often be specified by the user
        /// </remarks>
        /// <param name="carrier"></param>
        /// <param name="useParams"></param>
        /// <param name="args"></param>
        public static void SetParams(this USEParams.Carriers carrier, USEParams useParams, ConsoleArgs args)
        {
            USEDSAParams.DSASplitTypes splitType = args.SplitType;
            // parameters relevant for all carriers/sort types - values assigned are default unless otherwise specified

            useParams.Carrier = carrier;                                                            // Default is RoyalMail
            useParams.ServiceLevel = args.ServiceLevel;       
            useParams.ApplyRegionalSortRegardless = true;
            useParams.DirectResidueOrder = args.DirectResidueOrder;
            useParams.SortType = args.SortType;
            useParams.Piece = args.Piece;
            useParams.MailType = args.MailType;
            useParams.MinimumSelectionSize = args.MinimumSelectionSize;
            useParams.ItemWeight = args.ItemWeight;
            useParams.FixedWeight = true;
            useParams.MailingRef = args.MailingDesc;
            useParams.MailCellRef = "MailCellRef";
            useParams.MailCellDesc = "MailCellDesc";

            useParams.Container = args.Container;
            useParams.RetainUnsortedOrder = true;
            useParams.AllowAutoDpsLevelAdjustment = args.AllowAutoDpsLevelAdjustment;
            useParams.ApplyUnderSizedDirects = true;
            useParams.SortIslandBagsToEnd = true;
            useParams.AutoInjectRmAdvMailSeed = true;
            useParams.PostalArea = "CR";
            useParams.RejectCode = "99999";
            useParams.MaxContainerContentWeight = args.MaxContainerContentWeight;
            useParams.TrayAverageItems = 50;
            useParams.MaxTrayItemsWiggleRoom = 5;
            useParams.MaxAcceptableRejectPercentage = 10;
            useParams.Sustainability = args.Sustainabilities;
            useParams.StandardTariffBasis = USEParams.StandardTarriffBiasies.Account;
            useParams.Unsorted = args.Unsorted;
            useParams.AllowSelectionTrayWithdrawl = args.AllowSelectionTrayWithdrawal;
            useParams.WarnOnReportMissingAdvMailSeed = false;
            useParams.Sort48 = args.Sort48;
            useParams.MixedWidth = false;
            useParams.JicOptOut = false;
            useParams.Partial = args.Partial;
            useParams.SwitchOffVolumeChecking = args.SwitchOffVolumeChecking;
            useParams.WeightField = null;
            useParams.BagLabelsFilename = "xyz";                            // Must be specified for UKMail - used as prefix for bag filename
            useParams.Client = null;
            useParams.JobReference = null;
            useParams.JobDescription = "null";                              // Must be specified
            useParams.JicReference = null;

            // parameters only for DSA Carriers
            if (useParams is USEDSAParams dsaParams)
            {
                dsaParams.DSASplitType = splitType;
                dsaParams.MailingDate = DateTime.Now.ToString("yyyyMMdd");
                dsaParams.DSAAccountNo = "1234";
                dsaParams.DSADepot = "Bristol";                 // valid entries are Bedford, Bolton, Bristol or Glasgow
                dsaParams.WhistlMailingHouse = "A";
                dsaParams.WhistlPoNumber = "12345";
                dsaParams.WhistlSegmentCode = "AA";
                dsaParams.CitipostWhistlAccountZonal = "B2";
                dsaParams.CitipostWhistlAccountNational = "C3";
                dsaParams.OnePostAccountNational = null;
                dsaParams.OnePostAccountZonal = null;
                dsaParams.UCIDNational = null;
                dsaParams.UCIDZonal = null;
                dsaParams.OnePostPosterNational = null;
                dsaParams.OnePostPosterZonal = null;
                dsaParams.OnePostCarrierNational = null;
                dsaParams.OnePostCarrierZonal = null;
                dsaParams.OnePostCustomerNational = null;
                dsaParams.OnePostCustomerZonal = null;
                dsaParams.OnePostMailingHouse = null;
                dsaParams.OnePostAddress1 = null;
                dsaParams.OnePostAddress2 = null;
                dsaParams.OnePostAddress3 = null;
                dsaParams.OnePostTown = null;
                dsaParams.OnePostPostcode = null;
                dsaParams.OnePostPhoneNumber = null;
                dsaParams.OnePostSender = null;
                dsaParams.OnePostNationalBagStart = 0;
                dsaParams.OnePostZonalBagStart = 0;
                dsaParams.DSAMachinable = args.DSAMachinable;
                dsaParams.DSAPremium = false;
                dsaParams.DSASplit = false;
                dsaParams.DSAHighOnly = false;
                dsaParams.DSAUndersizedContainersAsStl = false;
                dsaParams.UKMailEconomyRejectTariff = false;
                dsaParams.DsaMinimumVolumeChecking = true;
                dsaParams.OnePostAgency = false;
            }

            switch (useParams)
            {
                // Parameters only for DSA MailMark parameters
                case USEDSAMailmarkParams dsaMailMarkParams:
                    dsaMailMarkParams.SCIDNational = args.NationalSupplyChainId;// null;
                    dsaMailMarkParams.SCIDZonal = args.ZonalSupplyChainId;//null;
                    dsaMailMarkParams.Owner = null;
                    dsaMailMarkParams.Payer = null;
                    dsaMailMarkParams.Producer = null;

                    // NB We use he same value for both these parameters in this demo although it is possible for them to be different
                    dsaMailMarkParams.BatchRef = args.MailmarkBatchReference;           // This is used as the middle part of the gemma file name SCID + this + Volume
                    dsaMailMarkParams.UniqueBatchRef = dsaMailMarkParams.BatchRef;      // This goes in the <batch-reference> tag of the gemma file

                    dsaMailMarkParams.Originator = null;
                    dsaMailMarkParams.Campaign = "null";                    // At the moment - Must be specified
                    dsaMailMarkParams.Department = null;
                    dsaMailMarkParams.CustomerRef = args.CustomerReferenceField;
                    dsaMailMarkParams.Spare2 = null;
                    dsaMailMarkParams.Spare3 = null;
                    dsaMailMarkParams.Spare4 = null;
                    dsaMailMarkParams.Spare5 = null;
                    dsaMailMarkParams.Spare6 = null;
                    dsaMailMarkParams.Spare7 = null;
                    dsaMailMarkParams.Spare8 = null;
                    dsaMailMarkParams.Spare10 = null;
                    dsaMailMarkParams.AdvertisingAttribute1 = null;
                    dsaMailMarkParams.AdvertisingAttribute2 = null;
                    dsaMailMarkParams.AdvertisingAttribute3 = null;
                    dsaMailMarkParams.AdvertisingAttribute4 = null;
                    dsaMailMarkParams.MailCellXmlExtension = null;
                    dsaMailMarkParams.ReturnPostcode = null;
                    dsaMailMarkParams.StartingBarcodeValueNational = 0;
                    dsaMailMarkParams.StartingBarcodeValueZonal = 0;
                    dsaMailMarkParams.VolumeOverride = false;
                    dsaMailMarkParams.Spare1 = false;
                    dsaMailMarkParams.MailSubType1 = null;
                    dsaMailMarkParams.MailSubType2 = null;
                    dsaMailMarkParams.MailSubType3 = null;
                    dsaMailMarkParams.MailSubType4 = false;
                    dsaMailMarkParams.ReturnPostcodeRequired = false;
                    dsaMailMarkParams.ForceSpare10 = false;
                    dsaMailMarkParams.UKMailEconomyRejectTariff = false;
                    dsaMailMarkParams.DSASubscriptionNewTitle = false;
                    break;
                case USEMailmarkParams mailMarkParams:
                    mailMarkParams.SCID = args.NationalSupplyChainId;//"1234567";                           // Must be specified
                    mailMarkParams.Owner = null;
                    mailMarkParams.Payer = null;
                    mailMarkParams.Producer = null;

                    // NB We use he same value for both these parameters in this demo although it is possible for them to be different
                    mailMarkParams.BatchRef = args.MailmarkBatchReference;           // Used as the middle part of the gemma file name SCID + this + Volume
                    mailMarkParams.UniqueBatchRef = mailMarkParams.BatchRef;         // Goes in the <Batch-Reference> tag of the gemma file

                    mailMarkParams.Originator = null;
                    mailMarkParams.Campaign = null;
                    mailMarkParams.Department = null;
                    mailMarkParams.CustomerRef = args.CustomerReferenceField;
                    mailMarkParams.Spare2 = null;
                    mailMarkParams.Spare3 = null;
                    mailMarkParams.Spare4 = null;
                    mailMarkParams.Spare5 = null;
                    mailMarkParams.Spare6 = null;
                    mailMarkParams.Spare7 = null;
                    mailMarkParams.Spare8 = null;
                    mailMarkParams.Spare10 = null;
                    mailMarkParams.AdvertisingAttribute1 = null;
                    mailMarkParams.AdvertisingAttribute2 = null;
                    mailMarkParams.AdvertisingAttribute3 = null;
                    mailMarkParams.AdvertisingAttribute4 = null;
                    mailMarkParams.MailCellXmlExtension = null;
                    mailMarkParams.ReturnPostcode = null;
                    mailMarkParams.StartingBarcodeValue = 0;
                    mailMarkParams.VolumeOverride = false;
                    mailMarkParams.Spare1 = false;
                    mailMarkParams.MailSubType1 = null;
                    mailMarkParams.MailSubType2 = null;
                    mailMarkParams.MailSubType3 = null;
                    mailMarkParams.MailSubType4 = true;
                    mailMarkParams.ReturnPostcodeRequired = false;
                    mailMarkParams.ForceSpare10 = false;
                    break;
            }
        }
    }
}
