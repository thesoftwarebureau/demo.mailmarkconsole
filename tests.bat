DSAMailMarkConsole /carrier=royalmail /n=6000000 /split=national

DSAMailMarkConsole /carrier=whistl /n=1000000 /split=national
DSAMailMarkConsole /carrier=whistl /z=1100000 /split=zonal

DSAMailMarkConsole /carrier=securedmail /n=2000000 /split=national
DSAMailMarkConsole /carrier=securedmail /z=2100000 /split=zonal

DSAMailMarkConsole /carrier=ukmail /z=3100000 /n=3000000 /split=arbitrage
DSAMailMarkConsole /carrier=ukmail /n=3000000 /split=national
DSAMailMarkConsole /carrier=ukmail /z=3100000 /split=zonal

DSAMailMarkConsole /carrier=citipost /z=4100000 /n=4000000 /split=arbitrage
DSAMailMarkConsole /carrier=citipost /n=4000000 /split=national
DSAMailMarkConsole /carrier=citipost /z=4100000 /split=zonal